package log

import (
	"log"
	"os"
)

const logPrefix = "[renovafacil] "
const logFlags = log.LstdFlags | log.Lmicroseconds

var logger = log.New(os.Stdout, logPrefix, logFlags)
var errLogger = log.New(os.Stderr, logPrefix, logFlags)

func Println(v ...interface{}) {
	logger.Println(v...)
}

func Printf(pattern string, params ...interface{}) {
	logger.Printf(pattern, params...)
}

func Errln(v ...interface{}) {
	errLogger.Println(v...)
}

func Errf(pattern string, params ...interface{}) {
	errLogger.Printf(pattern, params...)
}

func Fatalf(pattern string, params ...interface{}) {
	errLogger.Fatalf(pattern, params...)
}

func Fatalln(v ...interface{}) {
	errLogger.Fatalln(v...)
}
