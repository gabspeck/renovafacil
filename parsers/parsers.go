package parsers

import (
	"fmt"

	"bitbucket.org/gabspeck/renovafacil/models"
)

const (
	TypeIDAuto                  = "31"
	TypeIDCivilLiabilityAuto    = "53"
	TypeIDCartaVerde            = "25"
	TypeIDHome                  = "14"
	TypeIDCorporate             = "18"
	TypeIDCivilLiabilityGeneral = "51"
	TypeIDLossOfProfits         = "41"
)

const (
	TypeNameAuto                  = "Auto"
	TypeNameCivilLiabilityAuto    = "RCF-V"
	TypeNameCartaVerde            = "Carta Verde"
	TypeNameHome                  = "Residência"
	TypeNameCorporate             = "Empresarial"
	TypeNameCivilLiabilityGeneral = "RC Geral"
	TypeNameLossOfProfits         = "Lucros cessantes"
)

var insuranceTypes = map[string]string{
	TypeIDHome:                  TypeNameHome,
	TypeIDCartaVerde:            TypeNameCartaVerde,
	TypeIDAuto:                  TypeNameAuto,
	TypeIDCivilLiabilityAuto:    TypeNameCivilLiabilityAuto,
	TypeIDCorporate:             TypeNameCorporate,
	TypeIDCivilLiabilityGeneral: TypeNameCivilLiabilityGeneral,
	TypeIDLossOfProfits:         TypeNameLossOfProfits,
}

var parsers = map[string]PolicyParserFunc{}

type ParseResult struct {
	*models.Operation
	models.PolicySlice
}

type PolicyParserFunc func(*models.Account, []byte) (models.PolicySlice, error)

func GetInsuranceTypeName(id string) (name string, exists bool) {
	name, exists = insuranceTypes[id]
	return
}

func RegisterParser(key string, f PolicyParserFunc) {
	parsers[key] = f
}

func ParseQueryResults(account *models.Account, result *models.QueryResult, resultCh chan<- *ParseResult) {
	parserFn, err := Get(result.Account.Company.Name)
	if err != nil {
		resultCh <- &ParseResult{
			Operation: &models.Operation{
				Account:     result.Account,
				QueryParams: result.QueryParams,
				Err:         err,
			},
		}
		return
	}
	pols, err := parserFn(account, result.File)
	resultCh <- &ParseResult{
		Operation: &models.Operation{
			Account:     result.Account,
			QueryParams: result.QueryParams,
			Err:         err,
		},
		PolicySlice: pols,
	}
}

func Get(key string) (PolicyParserFunc, error) {
	f, ok := parsers[key]
	if !ok {
		return nil, fmt.Errorf("no parser found with key '%s'", key)
	}
	return f, nil
}
