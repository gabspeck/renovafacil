package parsers

import (
	"fmt"
	"sort"
)

type QueryResults map[int][]byte

func (qr QueryResults) AddPage(seq int, p []byte) {
	qr[seq] = p
}

func (qr QueryResults) String() string {
	keys := make([]int, len(qr))
	var i int
	for k := range qr {
		keys[i] = k
		i++
	}
	sort.Ints(keys)
	var s string
	for _, k := range keys {
		s += fmt.Sprintf("%d: %s; ", k, string(qr[k]))
	}
	return s
}
