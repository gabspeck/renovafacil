package parsers

import (
	"errors"
	"sort"
	"testing"

	"github.com/kylelemons/godebug/pretty"

	"bitbucket.org/gabspeck/renovafacil/models"
)

func TestParseQueryResults(t *testing.T) {
	oldParsers := parsers
	defer func() {
		parsers = oldParsers
	}()
	mockAccount := &models.Account{
		Name: "mock",
		Company: &models.Company{
			Name: "mock",
		},
	}
	parsers = map[string]PolicyParserFunc{
		"mock": mockParser,
	}

	type test struct {
		qr   *models.QueryResult
		pols models.PolicySlice
		err  error
	}

	tests := map[string]test{
		"valid policies": {
			qr: &models.QueryResult{
				Operation: &models.Operation{
					Account: mockAccount,
					Err:     nil,
				},
				File: []byte("page 1"),
			},
			pols: models.PolicySlice{
				models.Policy{
					Number: "POL001",
					Account: &models.Account{
						Name: "mock",
						Company: &models.Company{
							Name: "mock",
						},
					},
				},
				models.Policy{
					Number: "POL002",
					Account: &models.Account{
						Name: "mock",
						Company: &models.Company{
							Name: "mock",
						},
					},
				},
				models.Policy{
					Number: "POL003",
					Account: &models.Account{
						Name: "mock",
						Company: &models.Company{
							Name: "mock",
						},
					},
				},
			},
		},
	}
	for k, tst := range tests {
		ch := make(chan *ParseResult)
		go ParseQueryResults(mockAccount, tst.qr, ch)
		var parseResult = <-ch
		sort.Sort(tst.pols)
		sort.Sort(parseResult.PolicySlice)
		if c := pretty.Compare(tst.err, parseResult.Err); c != "" {
			t.Errorf("%s: wanted error %s, got %s", k, tst.err, parseResult.Err)
		}
		if c := pretty.Compare(tst.pols, parseResult.PolicySlice); c != "" {
			t.Errorf("%s: policies differ:\n%s", k, c)
		}
	}

}

func mockParser(a *models.Account, pg []byte) (models.PolicySlice, error) {
	var pols models.PolicySlice
	switch string(pg) {
	case "page 1":
		pols = models.PolicySlice{
			models.Policy{Number: "POL001", Account: a},
			models.Policy{Number: "POL002", Account: a},
			models.Policy{Number: "POL003", Account: a},
		}
	default:
		return nil, errors.New("invalid file")
	}
	return pols, nil
}
