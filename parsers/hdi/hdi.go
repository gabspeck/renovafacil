package hdi

import (
	"bufio"
	"bytes"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/gabspeck/renovafacil/models"
	"bitbucket.org/gabspeck/renovafacil/parsers"
	"golang.org/x/text/encoding/charmap"
	"golang.org/x/text/transform"
	"strconv"
)

func init() {
	parsers.RegisterParser("HDI", parseResultPage)
}

func parseResultPage(a *models.Account, b []byte) (models.PolicySlice, error) {
	const (
		rowTypeHeader       = "01"
		rowTypePolicyholder = "02"
		rowTypeItemAuto     = "03"
		rowTypeBrokerData   = "12"
		rowTypeItemLocation = "13"
	)
	const (
		//Policy header
		idxRowType     = 0
		idxNumber      = 1
		idxPeriodStart = 7
		idxPeriodEnd   = 8

		//Policyholder
		idxName  = 2
		idxPhone = 10
		idxTaxId = 12
		idxEmail = 13

		//Broker data
		idxType = 2

		//Items
		idxItemSeq = 2

		//Auto item
		idxMake      = 3
		idxModel     = 4
		idxMfgYear   = 5
		idxModelYear = 6

		//Location item
		idxLocationActivity = 13

		dateFormat = "02/01/2006"
	)

	r := bytes.NewReader(b)
	rt := transform.NewReader(r, charmap.Windows1252.NewDecoder())
	s := bufio.NewScanner(rt)
	polMap := map[string]*models.Policy{}
	for s.Scan() {
		l := s.Text()
		fields := strings.Split(l, ";")
		policyNumber := fields[idxNumber]
		var p *models.Policy
		var exists bool
		if p, exists = polMap[policyNumber]; !exists {
			p = &models.Policy{
				Account:   a,
				AccountID: a.ID,
			}
			polMap[policyNumber] = p
		}
		switch fields[idxRowType] {
		case rowTypeHeader:
			{
				p.Number = policyNumber
				p.RootNumber = policyNumber[0:15]
				p.EndorsementSequence, _ = strconv.Atoi(policyNumber[15:21])
				p.PeriodStart, _ = time.ParseInLocation(dateFormat, fields[idxPeriodStart], time.Local)
				p.PeriodEnd, _ = time.ParseInLocation(dateFormat, fields[idxPeriodEnd], time.Local)
			}
		case rowTypePolicyholder:
			{
				p.Customer = &models.Customer{
					Name:        removeRedundantWhitespace(fields[idxName]),
					TaxID:       removeRedundantWhitespace(fields[idxTaxId]),
					PhoneNumber: removeRedundantWhitespace(fields[idxPhone]),
				}
				if email := removeRedundantWhitespace(fields[idxEmail]); email != "" {
					p.Customer.Email.Scan(email)
				}
			}
		case rowTypeBrokerData:
			{
				typeID := fields[idxType]
				if p.InsuranceType == nil {
					p.InsuranceType = &models.InsuranceType{}
				}
				p.InsuranceType.Name = hdiParseType(p.InsuranceType.Name, typeID)
			}
		case rowTypeItemAuto:
			{
				if p.Description == "" {
					if itemSeq, _ := strconv.Atoi(fields[idxItemSeq]); itemSeq > 1 {
						p.Description = "FROTA"
					} else {
						p.Description = fmt.Sprintf(
							"%s %s %s/%s",
							removeRedundantWhitespace(fields[idxMake]),
							removeRedundantWhitespace(fields[idxModel]),
							removeRedundantWhitespace(fields[idxMfgYear]),
							removeRedundantWhitespace(fields[idxModelYear]))
					}
				}
			}
		case rowTypeItemLocation:
			{
				if p.Description == "" {
					p.Description = strings.TrimSpace(fields[idxLocationActivity])
				}
			}
		}
	}
	pols := make(models.PolicySlice, len(polMap))
	i := 0
	for _, v := range polMap {
		pols[i] = *v
		i++
	}
	return pols, nil
}

func hdiParseType(prevTypeName, newTypeID string) string {
	newTypeName, typeExists := parsers.GetInsuranceTypeName(newTypeID)
	if !typeExists {
		newTypeName = newTypeID
	}
	if (prevTypeName == parsers.TypeNameAuto && newTypeID != parsers.TypeIDCartaVerde) ||
		(prevTypeName == parsers.TypeNameCartaVerde || prevTypeName == parsers.TypeNameCorporate) {
		return prevTypeName
	}

	return newTypeName
}

func removeRedundantWhitespace(s string) string {
	return strings.Join(strings.Fields(s), " ")
}
