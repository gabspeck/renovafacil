package hdi

import (
	"database/sql"
	"io/ioutil"
	"reflect"
	"sort"
	"testing"
	"time"

	"bitbucket.org/gabspeck/renovafacil/models"
	"github.com/kylelemons/godebug/diff"
	"github.com/kylelemons/godebug/pretty"
)

func TestParseHDIResultPage(t *testing.T) {
	type getPoliciesTestParams struct {
		filename string
		policies models.PolicySlice
		err      error
	}

	hdiAccount := &models.Account{Entity: models.Entity{ID: 1}, Name: "HDI"}

	tests := map[string]getPoliciesTestParams{
		"01/03/2017 - 08/03/2017": {
			filename: "testdata/C100123456_apolices_emitidas_1491175535610.txt",
			policies: models.PolicySlice{
				{
					Number:              "01098133A001234000000",
					RootNumber:          "01098133A001234",
					EndorsementSequence: 0,
					Account:             hdiAccount,
					AccountID:           hdiAccount.ID,
					Customer: &models.Customer{
						Name:        "James Bond",
						Email:       sql.NullString{"atendimento@corretoradeseguros.com.br", true},
						PhoneNumber: "(0033)43210123",
						TaxID:       "60478747949",
					},
					Description:   "RENAULT DUSTER DYNAMIQUE 2.0 16V AUT 2016/2017",
					PeriodStart:   time.Date(2017, 3, 9, 0, 0, 0, 0, time.Local),
					PeriodEnd:     time.Date(2017, 3, 12, 0, 0, 0, 0, time.Local),
					InsuranceType: &models.InsuranceType{Name: "Carta Verde"},
				},
				{
					Number:              "01098431A432100000001",
					RootNumber:          "01098431A432100",
					EndorsementSequence: 1,
					Account:             hdiAccount,
					AccountID:           hdiAccount.ID,
					Customer: &models.Customer{
						Name:        "MULHER MARAVILHA",
						PhoneNumber: "(0047)36515500",
						TaxID:       "63235366920",
					},
					Description:   "FIAT TORO FREEDOM 1.8 16V FLEX AUT 2017/2017",
					PeriodStart:   time.Date(2017, 3, 3, 0, 0, 0, 0, time.Local),
					PeriodEnd:     time.Date(2017, 4, 12, 0, 0, 0, 0, time.Local),
					InsuranceType: &models.InsuranceType{Name: "Auto"},
				},
				{
					Number:              "01098431A112233000000",
					RootNumber:          "01098431A112233",
					EndorsementSequence: 0,
					Account:             hdiAccount,
					AccountID:           hdiAccount.ID,
					Customer: &models.Customer{
						Name:        "BETTE DAVIS",
						PhoneNumber: "(0033)42821623",
						TaxID:       "00442948913",
					},
					Description:   "TOYOTA COROLLA XEi 2.0 FLEX 16V AUT. 2014/2015",
					PeriodStart:   time.Date(2017, 2, 27, 0, 0, 0, 0, time.Local),
					PeriodEnd:     time.Date(2018, 2, 27, 0, 0, 0, 0, time.Local),
					InsuranceType: &models.InsuranceType{Name: "Auto"},
				},
				{
					Number:              "01098431A987654000000",
					RootNumber:          "01098431A987654",
					EndorsementSequence: 0,
					Account:             hdiAccount,
					AccountID:           hdiAccount.ID,
					Customer: &models.Customer{
						Name:        "CLARK KENT",
						PhoneNumber: "(0041)99999977",
						TaxID:       "19229907990",
					},
					Description:   "VOLKSWAGEN FOX 1.0 MI TOTAL FLEX 8V 5P 2013/2014",
					PeriodStart:   time.Date(2017, 3, 1, 0, 0, 0, 0, time.Local),
					PeriodEnd:     time.Date(2018, 3, 1, 0, 0, 0, 0, time.Local),
					InsuranceType: &models.InsuranceType{Name: "Auto"},
				},
			},
			err: nil,
		},
		"Residência": {
			filename: "testdata/residencia.txt",
			policies: models.PolicySlice{
				{
					Number:              "01098005A111111111111",
					RootNumber:          "01098005A111111",
					EndorsementSequence: 111111,
					Account:             hdiAccount,
					AccountID:           hdiAccount.ID,
					Customer: &models.Customer{
						Name:        "MICHAEL PHELPS",
						PhoneNumber: "(0099)55512345",
						TaxID:       "00888888888",
					},
					Description:   "RESIDENCIA HABITUAL APARTAMENTO",
					PeriodStart:   time.Date(2016, 05, 12, 0, 0, 0, 0, time.Local),
					PeriodEnd:     time.Date(2017, 05, 12, 0, 0, 0, 0, time.Local),
					InsuranceType: &models.InsuranceType{Name: "Residência"},
				},
			},
		},
		"unknown insurance type": {
			filename: "testdata/unknown_type.txt",
			policies: models.PolicySlice{
				{
					Number:              "999999999999999999999",
					RootNumber:          "999999999999999",
					EndorsementSequence: 999999,
					Account:             hdiAccount,
					AccountID:           hdiAccount.ID,
					Customer: &models.Customer{
						Name:        "EMPRESA DE SERVICOS LTDA",
						PhoneNumber: "(0055)99999999",
						TaxID:       "88888888888888",
						Email:       sql.NullString{"atendimento@corretoradeseguros.com.br", true},
					},
					Description:   "ESCRITORIOS - SOBRADO, TERREO E 1o. PAVIMENTO",
					PeriodStart:   time.Date(2016, 10, 03, 0, 0, 0, 0, time.Local),
					PeriodEnd:     time.Date(2017, 10, 03, 0, 0, 0, 0, time.Local),
					InsuranceType: &models.InsuranceType{Name: "99"},
				},
			},
		},
		"empresarial": {
			filename: "testdata/empresarial.txt",
			policies: models.PolicySlice{
				{
					Number:              "999999999999999999999",
					RootNumber:          "999999999999999",
					EndorsementSequence: 999999,
					Account:             hdiAccount,
					AccountID:           hdiAccount.ID,
					Customer: &models.Customer{
						Name:        "EMPRESA DE SERVICOS LTDA",
						PhoneNumber: "(0055)99999999",
						TaxID:       "88888888888888",
						Email:       sql.NullString{"atendimento@corretoradeseguros.com.br", true},
					},
					Description:   "ESCRITORIOS - SOBRADO, TERREO E 1o. PAVIMENTO",
					PeriodStart:   time.Date(2016, 10, 03, 0, 0, 0, 0, time.Local),
					PeriodEnd:     time.Date(2017, 10, 03, 0, 0, 0, 0, time.Local),
					InsuranceType: &models.InsuranceType{Name: "Empresarial"},
				},
			},
		},
		"empresarial com rc geral": {
			filename: "testdata/rc_geral.txt",
			policies: models.PolicySlice{
				{
					Number:              "111111111111111111111",
					RootNumber:          "111111111111111",
					EndorsementSequence: 111111,
					Account:             hdiAccount,
					AccountID:           hdiAccount.ID,
					Customer: &models.Customer{
						Name:        "AAAAAA BBBB",
						PhoneNumber: "(5555)44444444",
						TaxID:       "33333333333",
						Email:       sql.NullString{"hhhhhh@iiiiiiiiiii.com.br", true},
					},
					Description:   "EDIFICIOS DESOCUPADOS COM CLAUSULA DE EDIF DESOC",
					PeriodStart:   time.Date(2016, 06, 01, 0, 0, 0, 0, time.Local),
					PeriodEnd:     time.Date(2017, 06, 01, 0, 0, 0, 0, time.Local),
					InsuranceType: &models.InsuranceType{Name: "Empresarial"},
				},
			},
		},
		"empresarial com lucros cessantes": {
			filename: "testdata/lucros_cessantes.txt",
			policies: models.PolicySlice{
				{
					Number:              "111111111111111111111",
					RootNumber:          "111111111111111",
					EndorsementSequence: 111111,
					Account:             hdiAccount,
					AccountID:           hdiAccount.ID,
					Customer: &models.Customer{
						Name:        "AAAAAA BBBB",
						PhoneNumber: "(5555)44444444",
						TaxID:       "33333333333",
						Email:       sql.NullString{"hhhhhh@iiiiiiiiiii.com.br", true},
					},
					Description:   "EDIFICIOS DESOCUPADOS COM CLAUSULA DE EDIF DESOC",
					PeriodStart:   time.Date(2016, 06, 01, 0, 0, 0, 0, time.Local),
					PeriodEnd:     time.Date(2017, 06, 01, 0, 0, 0, 0, time.Local),
					InsuranceType: &models.InsuranceType{Name: "Empresarial"},
				},
			},
		},
		"endosso auto frota": {
			filename: "testdata/endosso_frota.txt",
			policies: models.PolicySlice{
				{
					Number:              "02029242A131932000002",
					RootNumber:          "02029242A131932",
					EndorsementSequence: 2,
					Account:             hdiAccount,
					AccountID:           hdiAccount.ID,
					Customer: &models.Customer{
						Name:        "SORVETERIA DO PINTO LTDA",
						PhoneNumber: "(0041)99999999",
						TaxID:       "99999999999999",
						Email:       sql.NullString{"atendimento@corretoradeseguros.com.br", true},
					},
					Description:   "FROTA",
					PeriodStart:   time.Date(2016, time.October, 14, 0, 0, 0, 0, time.Local),
					PeriodEnd:     time.Date(2017, time.September, 15, 0, 0, 0, 0, time.Local),
					InsuranceType: &models.InsuranceType{Name: "Auto"},
				},
			},
		},
	}
	for name, params := range tests {

		b, err := ioutil.ReadFile(params.filename)
		if err != nil {
			t.Errorf("could not read test file %s: %s", params.filename, err)
			continue
		}
		actualPol, err := parseResultPage(hdiAccount, b)
		if actualErr, expectedErr := err, params.err; actualErr != expectedErr {
			t.Errorf("%s:\nwanted:\n %s\ngot:\n%s\n", name, expectedErr, actualErr)
			continue
		}
		var lenActual, lenExpected int
		if lenActual, lenExpected = len(actualPol), len(params.policies); lenExpected != lenActual {
			t.Fatalf("%s: bad length. wanted: %d; got: %d.\n", name, lenExpected, lenActual)
		}
		sort.Sort(actualPol)
		sort.Sort(params.policies)
		for i := 0; i < lenExpected; i++ {
			a, e := actualPol[i], params.policies[i]
			if !reflect.DeepEqual(a, e) {
				t.Errorf("%s:\npolicies at position %d differ:\n%s", name, i, diff.Diff(pretty.Sprint(e), pretty.Sprint(a)))
			}
		}

	}
}
