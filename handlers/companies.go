package handlers

import (
	"net/http"

	"bitbucket.org/gabspeck/renovafacil/env"
	"bitbucket.org/gabspeck/renovafacil/models"
)

func CompaniesGet(env *env.Env, w http.ResponseWriter, r *http.Request) (interface{}, error) {
	var companies = []models.Company{}
	err := models.Companies(env.DB).Find().All(&companies)
	return companies, err
}
