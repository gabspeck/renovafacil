package handlers

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/gabspeck/renovafacil/env"
	"bitbucket.org/gabspeck/renovafacil/models"
)

const sessionName = "SESSION"

func NewPasswordPost(env *env.Env, w http.ResponseWriter, r *http.Request) (interface{}, error) {
	session, err := env.SessionStore.Get(r, sessionName)
	if err != nil {
		return nil, err
	}

	var form = map[string]string{}
	dec := json.NewDecoder(r.Body)
	if err := dec.Decode(&form); err != nil {
		return nil, err
	}
	userID := session.Values["userID"].(int64)
	if err := models.ChangeUserPassword(env.DB, userID, form["password"], form["confirmation"]); err != nil {
		return nil, err
	}

	return nil, nil

}

func LoginHandler(env *env.Env, w http.ResponseWriter, r *http.Request) (interface{}, error) {
	session, err := env.SessionStore.Get(r, sessionName)
	if err != nil {
		return nil, err
	}

	defer r.Body.Close()
	dec := json.NewDecoder(r.Body)
	loginReq := map[string]string{}
	if err := dec.Decode(&loginReq); err != nil {
		return nil, err
	}

	u, err := models.AuthenticateUser(env.DB, loginReq["username"], loginReq["password"])
	if err == models.ErrBadUsernameOrPassword {
		return nil, models.StatusError{http.StatusForbidden, err}
	}
	if err != nil {
		return nil, err
	}

	session.Values = map[interface{}]interface{}{
		"loggedIn": true,
		"userID":   u.ID,
	}

	session.Options.HttpOnly = false
	if err := env.SessionStore.Save(r, w, session); err != nil {
		return nil, err
	}

	return nil, nil
}

func LogoutHandler(env *env.Env, w http.ResponseWriter, r *http.Request) (interface{}, error) {
	session, err := env.SessionStore.Get(r, sessionName)
	if err != nil {
		return nil, err
	}
	session.Values = map[interface{}]interface{}{}
	session.Options.MaxAge = -1
	if err := env.SessionStore.Save(r, w, session); err != nil {
		return nil, err
	}
	return nil, nil
}
