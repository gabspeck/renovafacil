package handlers

import (
	"encoding/json"
	"math"
	"net/http"
	"strconv"

	"errors"

	"reflect"

	"bitbucket.org/gabspeck/renovafacil/env"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"bitbucket.org/gabspeck/renovafacil/models"
)

const defaultResultsPerPage = 20

var authKey = [64]byte{95, 16, 2, 222, 49, 47, 65, 161, 126, 183, 174, 54, 114, 30, 224, 110,
	212, 237, 56, 128, 139, 51, 247, 56, 85, 182, 159, 164, 138, 122, 91, 25,
	43, 245, 119, 54, 222, 141, 239, 178, 139, 221, 205, 125, 166, 25, 158, 11,
	93, 202, 78, 228, 65, 205, 250, 223, 223, 39, 164, 46, 11, 229, 158, 185}

type EnvHandler interface {
	Env() *env.Env
	http.Handler
}

type JSON struct {
	E *env.Env
	H func(e *env.Env, w http.ResponseWriter, r *http.Request) (interface{}, error)
}

func (j JSON) Env() *env.Env {
	return j.E
}

func (j JSON) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ret, err := j.H(j.E, w, r)
	writeJSON(w, ret, err)
}

type PagedJSON struct {
	E *env.Env
	H func(env *env.Env, page int, resultsPerPage int, w http.ResponseWriter, r *http.Request) (int, interface{}, error)
}

func (h PagedJSON) Env() *env.Env {
	return h.E
}

func (h PagedJSON) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	page, _ := strconv.Atoi(mux.Vars(r)["page"])
	if page < 1 {
		writeJSON(w, nil, errors.New("bad page number"))
		return
	}
	var (
		resultsPerPage int
		err            error
	)
	if resultsPerPage, err = strconv.Atoi(r.URL.Query().Get("resultsPerPage")); err != nil {
		resultsPerPage = defaultResultsPerPage
	}
	total, ret, err := h.H(h.E, page, resultsPerPage, w, r)
	var (
		pageCount int
		value     map[string]interface{}
	)
	if err == nil {
		pageCount = int(math.Ceil(float64(total) / float64(resultsPerPage)))
		value = map[string]interface{}{"page": page, "pageCount": pageCount, "results": ret}
	}
	writeJSON(w, value, err)
}

func RequireAuth(h EnvHandler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var err error
		var s *sessions.Session
		if s, err = h.Env().SessionStore.Get(r, sessionName); err == nil {
			if s.Values["loggedIn"] == true {
				h.ServeHTTP(w, r)
				return
			}
			err = models.StatusError{http.StatusUnauthorized, errors.New("access denied")}
		}
		writeJSON(w, nil, err)
	}
}

func writeJSON(w http.ResponseWriter, m interface{}, err error) {
	statusErr := models.ParseError(err)
	var noContent bool
	var (
		status int
		value  interface{}
	)
	if err == nil {
		if m == nil || reflect.ValueOf(m).IsNil() {
			status = http.StatusNoContent
			noContent = true
		} else {
			status = http.StatusOK
		}
		value = m
	} else {
		status = statusErr.Status()
		value = statusErr
	}
	var enc *json.Encoder
	if !noContent {
		enc = json.NewEncoder(w)
		w.Header().Set("Content-Type", "application/json;charset=utf-8")
	}
	w.WriteHeader(status)
	if !noContent {
		decErr := enc.Encode(value)
		if decErr != nil {
			writeJSON(w, nil, decErr)
		}
	}
}
