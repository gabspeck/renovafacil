package handlers

import (
	"net/http"

	"github.com/gorilla/csrf"
)

func TokenGetHandler(w http.ResponseWriter, r *http.Request) {
	tok := csrf.Token(r)
	w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")
	w.Header().Set("X-CSRF-Token", tok)
	w.WriteHeader(http.StatusNoContent)
}
