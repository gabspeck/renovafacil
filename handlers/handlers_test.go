package handlers

import (
	"testing"

	"net/http"

	"errors"
	"net/http/httptest"

	"io/ioutil"

	"bitbucket.org/gabspeck/renovafacil/env"
	"bitbucket.org/gabspeck/renovafacil/models"
	"github.com/gorilla/sessions"
	"github.com/kylelemons/godebug/pretty"
	"upper.io/db.v3/lib/sqlbuilder"
)

func TestHandlers(t *testing.T) {
	setUpMocks()
	t.Run("LoginHandler", testLoginHandler)
	t.Run("LogoutHandler", testLogoutHandler)
	t.Run("AccountsGetHandler", testAccountsEnabledGet)
	t.Run("NewPasswordPost", testNewPasswordPost)
}

func createSession() *http.Cookie{
	sessionReq := httptest.NewRequest("GET", "/", nil)
	ss := env.Get().SessionStore
	s, _ := ss.New(sessionReq, sessionName)
	s.Values["userID"] = int64(1000)
	w := httptest.NewRecorder()
	ss.Save(sessionReq, w, s)
	cookies := w.Result().Cookies()
	for i := 0; i < len(cookies); i++ {
		if cookies[i].Name == sessionName {
			return cookies[i]
		}
	}
	return nil
}

func setUpMocks() {
	mockEnv := &env.Env{
		SessionStore: sessions.NewCookieStore(authKey[:]),
	}
	env.New = func() (*env.Env, error) {
		return mockEnv, nil
	}

	env.Get = func() *env.Env {
		return mockEnv
	}
	models.AuthenticateUser = func(dbase sqlbuilder.Database, username, password string) (*models.User, error) {
		if username == "albert" && password == "secret" {
			return &models.User{
				Entity:   models.Entity{ID: 1},
				Username: "albert",
				Hash:     []byte("this is a hash"),
				FullName: "Doctor Albert",
			}, nil
		}
		return nil, models.ErrBadUsernameOrPassword
	}

	models.ChangeUserPassword = func(dbase sqlbuilder.Database, userID int64, password, confirmation string) error {
		if userID == 1000 && password == "newpass" && confirmation == "newpass" {
			return nil
		}
		return errors.New("backend error")
	}
}

func TestJSON_ServeHTTP(t *testing.T) {
	type Person struct {
		Name string
		Age  int
		City string
	}
	h := JSON{env.Get(), func(e *env.Env, w http.ResponseWriter, r *http.Request) (interface{}, error) {
		if r.URL.Path == "/users" {
			return nil, nil
		}
		if idSlice, hasID := r.URL.Query()["id"]; len(idSlice) > 0 && idSlice[0] == "3" {
			return &Person{
				Name: "Mr. Bob",
				Age:  52,
				City: "Cedar Rapids",
			}, nil
		} else if !hasID {
			return nil, errors.New("backend panic")
		}
		return nil, models.StatusError{http.StatusNotFound, errors.New("person not found")}
	}}

	tests := map[string]struct {
		method       string
		target       string
		payload      []byte
		statusCode   int
		responseBody string
	}{
		"valid request": {
			method:       "POST",
			target:       "/user?id=3",
			statusCode:   http.StatusOK,
			responseBody: `{"Name":"Mr. Bob","Age":52,"City":"Cedar Rapids"}` + "\n",
		},
		"entity not found": {
			method:       "POST",
			target:       "/user?id=9",
			statusCode:   http.StatusNotFound,
			responseBody: `{"message":"person not found"}` + "\n",
		},
		"backend error": {
			method:       "POST",
			target:       "/user",
			statusCode:   http.StatusInternalServerError,
			responseBody: `{"message":"backend panic"}` + "\n",
		},
		"no content": {
			method:       "GET",
			target:       "/users",
			statusCode:   http.StatusNoContent,
			responseBody: "",
		},
	}

	for k, test := range tests {
		w := httptest.NewRecorder()
		r := httptest.NewRequest(test.method, test.target, nil)
		h.ServeHTTP(w, r)
		if sc := w.Result().StatusCode; sc != test.statusCode {
			t.Errorf("JSON.ServeHTTP [%s]: wanted status code %d, got %d", k, test.statusCode, sc)
		}
		b := w.Result().Body
		defer b.Close()
		bodyBytes, _ := ioutil.ReadAll(b)
		if d := pretty.Compare(test.responseBody, string(bodyBytes)); d != "" {
			t.Errorf("JSON.ServeHTTP [%s]: bad return body:\n%s", k, d)
		}
	}
}
