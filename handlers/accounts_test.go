package handlers

import (
	"net/http"
	"testing"

	"net/http/httptest"

	"bitbucket.org/gabspeck/renovafacil/env"
	"bitbucket.org/gabspeck/renovafacil/models"
	"github.com/kylelemons/godebug/pretty"
	"upper.io/db.v3/lib/sqlbuilder"
)

type accountTest struct {
	fn  func(sqlbuilder.Database, bool) ([]models.Account, error)
	ret []models.Account
	err error
}

var noEnabledAccountsFunc = func(sqlbuilder.Database, bool) ([]models.Account, error) {
	return nil, nil
}

var enabledAccountsFunc = func(dbase sqlbuilder.Database, enabledOnly bool) ([]models.Account, error) {
	var enabled = []models.Account{
		{
			Entity:    models.Entity{ID: 1},
			CompanyID: 1,
			Name:      "AIG",
			Username:  "aig",
			Password:  "aigpass",
			Enabled:   true,
			Company: &models.Company{
				Entity: models.Entity{ID: 1},
				Name:   "AIG",
			},
		},
		{
			Entity:    models.Entity{ID: 3},
			CompanyID: 2,
			Name:      "AXA",
			Enabled:   true,
			Username:  "axauser",
			Password:  "eugene",
			Company: &models.Company{
				Entity: models.Entity{ID: 2},
				Name:   "AXA",
			},
		},
		{
			Entity:    models.Entity{ID: 4},
			CompanyID: 3,
			Name:      "MetLife",
			Enabled:   true,
			Username:  "metuser",
			Password:  "bbad",
			Company: &models.Company{
				Entity: models.Entity{ID: 3},
				Name:   "MetLife",
			},
		},
	}
	if enabledOnly {
		return enabled, nil
	}

	var disabled = []models.Account{
		{
			Entity:    models.Entity{ID: 2},
			CompanyID: 2,
			Name:      "Reinholm",
			Enabled:   false,
			Username:  "roy",
			Password:  "imdisabled",
		},
	}

	return append(enabled, disabled...), nil
}

func testAccountsEnabledGet(t *testing.T) {
	tests := map[string]accountTest{
		"no enabled accounts": {
			fn:  noEnabledAccountsFunc,
			ret: nil,
			err: nil,
		},
		"has enabled accounts": {
			fn: enabledAccountsFunc,
			ret: []models.Account{
				{
					Entity:    models.Entity{ID: 1},
					CompanyID: 1,
					Name:      "AIG",
					Username:  "aig",
					Password:  "aigpass",
					Enabled:   true,
					Company: &models.Company{
						Entity: models.Entity{ID: 1},
						Name:   "AIG",
					},
				},
				{
					Entity:    models.Entity{ID: 3},
					CompanyID: 2,
					Name:      "AXA",
					Enabled:   true,
					Username:  "axauser",
					Password:  "eugene",
					Company: &models.Company{
						Entity: models.Entity{ID: 2},
						Name:   "AXA",
					},
				},
				{
					Entity:    models.Entity{ID: 4},
					CompanyID: 3,
					Name:      "MetLife",
					Enabled:   true,
					Username:  "metuser",
					Password:  "bbad",
					Company: &models.Company{
						Entity: models.Entity{ID: 3},
						Name:   "MetLife",
					},
				},
			},
			err: nil,
		},
	}

	for k, test := range tests {
		models.GetAccounts = test.fn
		w := httptest.NewRecorder()
		r := httptest.NewRequest(http.MethodGet, "/api/accounts/enabled", nil)
		ret, err := AccountsEnabledGet(env.Get(), w, r)
		if d := pretty.Compare(test.ret, ret); d != "" {
			t.Errorf("AccountsEnabledGet [%s]: returned value differs:\n%s", k, d)
		}
		if d := pretty.Compare(test.err, err); d != "" {
			t.Errorf("AccountsEnabledGet [%s]: returned error differs:\n%s", k, d)
		}
	}
}

func testAccountsGet(t *testing.T) {
	tests := map[string]accountTest{
		"no accounts": {
			fn:  noEnabledAccountsFunc,
			ret: nil,
			err: nil,
		},
		"enabled & disabled accounts": {
			fn: enabledAccountsFunc,
			ret: []models.Account{
				{
					Entity:    models.Entity{ID: 1},
					CompanyID: 1,
					Name:      "AIG",
					Username:  "aig",
					Password:  "aigpass",
					Enabled:   true,
				},
				{
					Entity:    models.Entity{ID: 3},
					CompanyID: 2,
					Name:      "AXA",
					Enabled:   true,
					Username:  "axauser",
					Password:  "eugene",
				},
				{
					Entity:    models.Entity{ID: 4},
					CompanyID: 3,
					Name:      "MetLife",
					Enabled:   true,
					Username:  "metuser",
					Password:  "bbad",
				},
				{
					Entity:    models.Entity{ID: 2},
					CompanyID: 2,
					Name:      "Reinholm",
					Enabled:   false,
					Username:  "roy",
					Password:  "imdisabled",
				},
			},
			err: nil,
		},
	}

	for k, test := range tests {
		models.GetAccounts = test.fn
		w := httptest.NewRecorder()
		r := httptest.NewRequest(http.MethodGet, "/api/accounts", nil)
		ret, err := AccountsGet(env.Get(), w, r)
		if d := pretty.Compare(test.ret, ret); d != "" {
			t.Errorf("GetAccounts [%s]: returned value differs:\n%s", k, d)
		}
		if d := pretty.Compare(test.err, err); d != "" {
			t.Errorf("GetAccounts [%s]: returned error differs:\n%s", k, d)
		}
	}
}
