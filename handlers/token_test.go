package handlers

import (
	"net/http/httptest"
	"testing"
	"github.com/gorilla/csrf"
	"net/http"
)

type h struct {
	f http.HandlerFunc
}

func (h h) ServeHTTP(w http.ResponseWriter, r *http.Request){
	h.f(w, r)
}

func TestTokenGetHandler(t *testing.T) {
	r := httptest.NewRequest("GET", "/api/token", nil)
	w := httptest.NewRecorder()
	csrf.Protect(authKey[:])(h{TokenGetHandler}).ServeHTTP(w, r)

	if token := w.Result().Header.Get("X-CSRF-Token"); token == "" {
		t.Errorf("TokenGetHandler: no token set")
	}
}
