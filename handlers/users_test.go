package handlers

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http/httptest"
	"testing"

	"net/http"
	"time"

	"strings"

	"bitbucket.org/gabspeck/renovafacil/env"
	"bitbucket.org/gabspeck/renovafacil/models"
	"github.com/gorilla/securecookie"
	"github.com/kylelemons/godebug/pretty"
)

func testLoginHandler(t *testing.T) {
	tests := map[string]struct {
		payload       map[string]interface{}
		sessionCookie bool
		err           error
	}{
		"correct username and password": {
			payload: map[string]interface{}{
				"username": "albert",
				"password": "secret",
			},
			sessionCookie: true,
			err:           nil,
		},
		"bad username": {
			payload: map[string]interface{}{
				"username": "notalbert",
				"password": "secret",
			},
			sessionCookie: false,
			err:           models.StatusError{403, errors.New("bad username or password")},
		},
	}
	for k, test := range tests {
		b, _ := json.Marshal(&test.payload)
		r := httptest.NewRequest("POST", "/login", bytes.NewReader(b))
		w := httptest.NewRecorder()
		e, _ := env.New()
		_, err := LoginHandler(e, w, r)
		if d := pretty.Compare(test.err, err); d != "" {
			t.Errorf("LoginHandler [%s]: error differs from expected:\n%s", k, d)
		}
		hasCookie := false
		for _, c := range w.Result().Cookies() {
			if c.Name == "SESSION" {
				hasCookie = true
				break
			}
		}
		if test.sessionCookie && !hasCookie {
			t.Errorf("LoginHandler [%s]: response has no session cookie, but should", k)
		} else if !test.sessionCookie && hasCookie {
			t.Errorf("LoginHandler [%s]: response has session cookie, but shouldn't", k)
		}
	}
}

func testLogoutHandler(t *testing.T) {
	rootReq := httptest.NewRequest(http.MethodGet, "/", nil)
	s, _ := env.Get().SessionStore.New(rootReq, "SESSION")
	s.Values["username"] = "somebody"
	rootW := httptest.NewRecorder()
	env.Get().SessionStore.Save(rootReq, rootW, s)
	r := httptest.NewRequest(http.MethodGet, "/logout", nil)
	r.AddCookie(rootW.Result().Cookies()[0])
	w := httptest.NewRecorder()
	_, err := LogoutHandler(env.Get(), w, r)
	if err != nil {
		t.Errorf("LogoutHandler: error: %s", err)
	}
	foundCookie := false
	for _, c := range w.Result().Cookies() {
		if c.Name == "SESSION" {
			foundCookie = true
			if !c.Expires.Before(time.Now()) {
				t.Errorf("LogoutHandler: session cookie not expired (expires: %s)", c.RawExpires)
			}
			codecs := securecookie.CodecsFromPairs(authKey[:])
			sessionValues := map[interface{}]interface{}{}
			securecookie.DecodeMulti(c.Name, c.Value, &sessionValues, codecs[0])
			if sv := sessionValues["loggedIn"]; sv != nil {
				t.Errorf("LogoutHandler: wrong value for loggedIn: %v", sv)
			}
		}
	}
	if !foundCookie {
		t.Errorf("LogoutHandler: session cookie untouched")
	}
}

func testNewPasswordPost(t *testing.T) {
	tests := map[string]struct {
		reqBody string
		err     error
	}{
		"valid request": {
			reqBody: `{"password": "newpass", "confirmation": "newpass"}`,
			err:     nil,
		},
		"bad request": {
			reqBody: `{"password": "newpass", "confirmation": "notthesame"}`,
			err:     errors.New("backend error"),
		},
	}

	for k, test := range tests {
		w := httptest.NewRecorder()
		r := httptest.NewRequest(http.MethodPost, "/newpassword", strings.NewReader(test.reqBody))
		r.AddCookie(createSession())
		b, err := NewPasswordPost(env.Get(), w, r)
		if b != nil {
			t.Errorf("NewPasswordPost [%s]: non-nil return", k)
		}
		if d := pretty.Compare(test.err, err); d != "" {
			t.Errorf("NewPasswordPost [%s]: errors differ:\n%s", k, d)
		}
	}
}
