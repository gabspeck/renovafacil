package handlers

import (
	"net/http"

	"encoding/json"
	"strconv"

	"bitbucket.org/gabspeck/renovafacil/env"
	"bitbucket.org/gabspeck/renovafacil/models"
	"github.com/gorilla/mux"
)

func AccountsEnabledGet(env *env.Env, w http.ResponseWriter, req *http.Request) (interface{}, error) {
	return models.GetAccounts(env.DB, true)
}

func AccountsGet(env *env.Env, w http.ResponseWriter, req *http.Request) (interface{}, error) {
	return models.GetAccounts(env.DB, false)
}

func AccountsPost(env *env.Env, w http.ResponseWriter, req *http.Request) (interface{}, error) {
	d := json.NewDecoder(req.Body)
	defer req.Body.Close()
	a := models.Account{}

	if err := d.Decode(&a); err != nil {
		return nil, err
	}
	a.Enabled = true
	_, err := models.AddAccount(env.DB, &a)
	return nil, err
}

func AccountsEditPost(env *env.Env, w http.ResponseWriter, req *http.Request) (interface{}, error) {
	d := json.NewDecoder(req.Body)
	defer req.Body.Close()
	a := models.Account{}
	if err := d.Decode(&a); err != nil {
		return nil, err
	}
	return nil, models.UpdateAccount(env.DB, &a)
}

func AccountsDelete(env *env.Env, w http.ResponseWriter, req *http.Request) (interface{}, error) {
	accountID, _ := strconv.ParseInt(mux.Vars(req)["id"], 10, 64)
	return nil, models.Accounts(env.DB).Find("id", accountID).Delete()
}
