package handlers

import (
	"bitbucket.org/gabspeck/renovafacil/env"
	"bitbucket.org/gabspeck/renovafacil/models"
	"net/http"
	"time"
	"encoding/json"
	"github.com/gorilla/mux"
	"strconv"
)

func RenewalsGetHandler(env *env.Env, page int, resultsPerPage int, w http.ResponseWriter, req *http.Request) (int, interface{}, error) {
	total, pols, err := models.GetPendingRenewalsByPeriodEnd(env.DB, uint(resultsPerPage), uint(page), time.Now())
	return total, pols, err
}

func HistoryGetHandler(env *env.Env, page int, resultsPerPage int, w http.ResponseWriter, r *http.Request) (int, interface{}, error) {
	return models.GetRenewalHistory(env.DB, uint(resultsPerPage), uint(page))
}

func RenewPostHandler(env *env.Env, w http.ResponseWriter, req *http.Request) (interface{}, error) {
	d := json.NewDecoder(req.Body)
	defer req.Body.Close()
	rl := &models.RenewalLog{}
	if err := d.Decode(rl); err != nil {
		return nil, err
	}

	rl.Date = time.Now().UTC()
	_, err := models.AddRenewalLog(env.DB, rl)

	return nil, err
}

func RenewalsDeleteHandler(env *env.Env, w http.ResponseWriter, req *http.Request) (interface{}, error) {
	logID, _ := strconv.ParseInt(mux.Vars(req)["id"], 10, 64)
	return nil, models.DeleteRenewalLogByID(env.DB, logID)
}
