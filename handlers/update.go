package handlers

import (
	"net/http"

	"bitbucket.org/gabspeck/renovafacil/env"
	"bitbucket.org/gabspeck/renovafacil/update"
)

func UpdateHandler(e *env.Env, w http.ResponseWriter, r *http.Request) (interface{}, error) {
	go update.Run(e.DB, e.QueryResultCh, e.ParseResultCh)
	return nil, nil
}
