FROM golang:latest
ENV TZ America/Sao_Paulo
WORKDIR /usr/src/app/
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone && \
    go get github.com/codegangsta/gin
ENTRYPOINT ["gin", "-i", "-p", "3000"]
