package update

import (
	"bitbucket.org/gabspeck/renovafacil/log"
	"bitbucket.org/gabspeck/renovafacil/models"
	"bitbucket.org/gabspeck/renovafacil/parsers"
	"upper.io/db.v3/lib/sqlbuilder"
	"time"
	"bitbucket.org/gabspeck/renovafacil/clients"
	"sync"
	"fmt"
)

var lock bool
var mutex sync.Mutex = sync.Mutex{}

func StartTicker(db sqlbuilder.Database, queryResultCh chan<- *models.QueryResult, parseResultCh <-chan *parsers.ParseResult) {
	dbTicker := time.NewTicker(time.Hour * 12)
	log.Println("database update daemon started")
	for range dbTicker.C {
		go Run(db, queryResultCh, parseResultCh)
	}
}

func Run(db sqlbuilder.Database, queryResultCh chan<- *models.QueryResult, parseResultCh <-chan *parsers.ParseResult) {
	if err := setLock(true); err != nil {
		log.Errln(err)
		return
	}
	defer setLock(false)
	log.Println("updating data")
	now := time.Now()
	yearAgo := now.AddDate(-1, 0, 0)
	var (
		accounts []models.Account
		err      error
	)
	if accounts, err = models.GetAccounts(db, true); err != nil {
		log.Errf("error fetching enabled accounts: %s", err)
		return
	}
	clients.DownloadPoliciesByDate(accounts, yearAgo, now, queryResultCh)
	log.Println("downloads done")
}

func Listen(queryResultCh chan *models.QueryResult, parseResultCh chan *parsers.ParseResult, db sqlbuilder.Database) {
	go func() {
		for r := range queryResultCh {
			if r.Err != nil {
				logOperationError("query", r.Operation)
				continue
			}
			go parsers.ParseQueryResults(r.Account, r, parseResultCh)
		}
	}()

	go func() {
		for r := range parseResultCh {
			if r.Err != nil {
				logOperationError("parse", r.Operation)
				continue
			}
			for _, p := range r.PolicySlice {
				if _, err := models.AddPolicy(db, &p); err != nil {
					log.Errf("%s: error adding policy: %s (account: %d; number: %s)", r.Account.Name, err, p.AccountID, p.Number)
				}
			}
		}
	}()
}

func setLock(l bool) error {
	mutex.Lock()
	defer mutex.Unlock()
	if l && lock {
		return fmt.Errorf("update in progress; aborting")
	}
	lock = l
	return nil
}

func logOperationError(opName string, o *models.Operation) {
	if o.QueryParams != nil {
		log.Errf("%s: %s (from: %s, until: %s, account: %s)", opName, o.Err, o.From, o.Until, o.Account.Name)
	} else {
		log.Errf("%s: %s (account: %s)", opName, o.Err, o.Account.Name)
	}
}
