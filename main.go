package main

import (
	"time"

	"fmt"
	"net/http"
	"os"

	"bitbucket.org/gabspeck/renovafacil/clients"
	_ "bitbucket.org/gabspeck/renovafacil/clients/all"
	"bitbucket.org/gabspeck/renovafacil/env"
	"bitbucket.org/gabspeck/renovafacil/handlers"
	"bitbucket.org/gabspeck/renovafacil/log"
	"bitbucket.org/gabspeck/renovafacil/models"
	_ "bitbucket.org/gabspeck/renovafacil/parsers/all"
	"bitbucket.org/gabspeck/renovafacil/update"
	"github.com/gorilla/csrf"
	"github.com/gorilla/mux"
)

var csrfProtectionKey = [32]byte{31, 45, 3, 238, 221, 8, 157, 107, 10, 68, 138, 236, 136, 241, 55, 2,
	2, 242, 135, 11, 153, 30, 47, 28, 65, 197, 147, 232, 70, 246, 169, 160}

func main() {
	var (
		e   *env.Env
		err error
	)
	if e, err = env.New(); err != nil {
		log.Fatalln(err)
	}
	if err := models.InitializeCompanies(e.DB, clients.GetClientKeys()); err != nil {
		log.Fatalf("error initializing companies; %s", err)
	}
	if p := os.Getenv("HTTP_PROXY"); p != "" {
		log.Printf("proxying HTTP requests through %s", p)
	}
	go startDaemons(e)
	defer e.DB.Close()
	log.Println("renovafacil is a go")
	log.Fatalln(setUpRouter(e))
}

func setUpRouter(e *env.Env) error {
	r := mux.NewRouter()
	r.StrictSlash(true)
	api := r.PathPrefix("/api/").Subrouter()
	apiGet := api.Methods(http.MethodGet).Subrouter()
	apiGet.Handle("/companies", handlers.RequireAuth(handlers.JSON{e, handlers.CompaniesGet}))
	apiGet.Handle("/renewals/{page:[0-9]+}", handlers.RequireAuth(handlers.PagedJSON{e, handlers.RenewalsGetHandler}))
	apiGet.Handle("/renewals/history/{page:[0-9]+}", handlers.RequireAuth(handlers.PagedJSON{e, handlers.HistoryGetHandler}))
	apiGet.HandleFunc("/update", handlers.RequireAuth(handlers.JSON{e, handlers.UpdateHandler}))
	apiGet.Handle("/accounts/enabled", handlers.RequireAuth(handlers.JSON{e, handlers.AccountsEnabledGet}))
	apiGet.Handle("/accounts", handlers.RequireAuth(handlers.JSON{e, handlers.AccountsGet}))
	apiGet.Handle("/logout", handlers.JSON{e, handlers.LogoutHandler})
	apiGet.HandleFunc("/token", handlers.TokenGetHandler)

	apiPost := api.Methods(http.MethodPost).Subrouter()
	apiPost.Handle("/login", handlers.JSON{e, handlers.LoginHandler})
	apiPost.Handle("/renew", handlers.RequireAuth(handlers.JSON{e, handlers.RenewPostHandler}))
	apiPost.Handle("/accounts", handlers.RequireAuth(handlers.JSON{e, handlers.AccountsPost}))
	apiPost.Handle("/accounts/edit", handlers.RequireAuth(handlers.JSON{e, handlers.AccountsEditPost}))
	apiPost.Handle("/newpassword", handlers.RequireAuth(handlers.JSON{e, handlers.NewPasswordPost}))

	apiDelete := api.Methods(http.MethodDelete).Subrouter()
	apiDelete.Handle("/renewals/{id:[0-9]+}", handlers.RequireAuth(handlers.JSON{e, handlers.RenewalsDeleteHandler}))
	apiDelete.Handle("/accounts/{id:[0-9]+}", handlers.RequireAuth(handlers.JSON{e, handlers.AccountsDelete}))

	srv := http.Server{
		Addr:         fmt.Sprintf(":%s", os.Getenv("PORT")),
		ReadTimeout:  15 * time.Second,
		WriteTimeout: 15 * time.Second,
		Handler:      csrf.Protect(csrfProtectionKey[:], csrf.HttpOnly(false), csrf.Secure(!e.DevMode))(r),
	}

	return srv.ListenAndServe()
}

func startDaemons(e *env.Env) {
	go update.Listen(e.QueryResultCh, e.ParseResultCh, e.DB)
	go update.StartTicker(e.DB, e.QueryResultCh, e.ParseResultCh)
}
