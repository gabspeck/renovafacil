module bitbucket.org/gabspeck/renovafacil

require (
	cloud.google.com/go v0.27.0 // indirect
	github.com/Microsoft/go-winio v0.4.11 // indirect
	github.com/PuerkitoBio/goquery v1.4.1
	github.com/andybalholm/cascadia v0.0.0-20161224141413-349dd0209470 // indirect
	github.com/antonlindstrom/pgstore v0.0.0-20170604072116-a407030ba6d0
	github.com/cznic/ql v1.2.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/denisenkom/go-mssqldb v0.0.0-20180901172138-1eb28afdf9b6 // indirect
	github.com/docker/distribution v2.6.2+incompatible // indirect
	github.com/docker/docker v1.13.1 // indirect
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/docker/go-units v0.3.3 // indirect
	github.com/go-sql-driver/mysql v1.4.0 // indirect
	github.com/google/go-cmp v0.2.0 // indirect
	github.com/gorilla/csrf v1.5.1
	github.com/gorilla/mux v1.6.2
	github.com/gorilla/securecookie v1.1.1
	github.com/gorilla/sessions v1.1.2
	github.com/guregu/null v3.2.0+incompatible
	github.com/kr/pretty v0.1.0 // indirect
	github.com/kylelemons/godebug v0.0.0-20170224010052-a616ab194758
	github.com/lib/pq v0.0.0-20170810061220-e42267488fe3 // indirect
	github.com/mattes/migrate v3.0.1+incompatible
	github.com/mattn/go-sqlite3 v1.9.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/stevvooe/resumable v0.0.0-20180830230917-22b14a53ba50 // indirect
	github.com/stretchr/testify v1.2.2 // indirect
	golang.org/x/crypto v0.0.0-20170916190215-7d9177d70076
	golang.org/x/net v0.0.0-20170809000501-1c05540f6879 // indirect
	golang.org/x/sys v0.0.0-20180909124046-d0be0721c37e // indirect
	golang.org/x/text v0.0.0-20170810154203-b19bf474d317
	google.golang.org/appengine v1.1.0 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/mgo.v2 v2.0.0-20180705113604-9856a29383ce // indirect
	gopkg.in/yaml.v2 v2.0.0-20170812160011-eb3733d160e7
	upper.io/db.v3 v3.5.4+incompatible
)
