package clients

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"
)

func TestNewScraper(t *testing.T) {
	s := NewScraper(nil, "")
	const expectedTimeout = 30 * time.Second
	if s.Timeout != expectedTimeout {
		t.Errorf("wrong timeout. wanted %d, got %d", expectedTimeout, s.Timeout)
	}
}

func TestScraper_Do(t *testing.T) {
	srv := newScraperTestServer()
	defer srv.Close()
	s := NewScraper(nil, "")
	req, _ := http.NewRequest("GET", srv.URL+"/", nil)
	res, err := s.Do(req)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	defer res.Body.Close()
	b, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Fatalf("error reading test server response: %s", err)
	}
	if res.StatusCode != http.StatusOK {
		t.Errorf("%s", string(b))
	}
}

func newScraperTestServer() *httptest.Server {
	return httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var acceptOK, userAgentOK bool
		h := r.Header
		accept, userAgent := h.Get("Accept"), h.Get("User-Agent")
		if acceptOK, userAgentOK =
			strings.Contains(accept, "text/html"), strings.Contains(userAgent, "Chrome"); acceptOK && userAgentOK {
			w.WriteHeader(http.StatusOK)
			return
		}
		w.WriteHeader(http.StatusBadRequest)
		if !acceptOK {
			w.Write([]byte(fmt.Sprintf("bad accept header: %s\n", accept)))
		}
		if !userAgentOK {
			w.Write([]byte(fmt.Sprintf("bad user agent: %s\n", userAgent)))
		}
	}))
}
