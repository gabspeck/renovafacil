package clients

import (
	"io"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"strings"
	"time"

	"bitbucket.org/gabspeck/renovafacil/models"
)

type Scraper struct {
	*http.Client
	Username string
	Password string
	BaseURL  string
}

type HTTPError struct {
	StatusCode int
	Status     string
}

func (e HTTPError) Error() string {
	return e.Status
}

var defaultHeaders = map[string]string{
	"Accept":     "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
	"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36",
}

func NewScraper(p models.ClientParams, defaultURL string) *Scraper {
	s := &Scraper{
		Client: &http.Client{
			Timeout: 30 * time.Second,
		},
	}
	var err error
	s.Jar, err = cookiejar.New(nil)
	if err != nil {
		panic(err)
	}
	if p != nil {
		s.Username = p["username"]
		s.Password = p["password"]
		s.BaseURL = p["base_url"]
	}
	if s.BaseURL == "" {
		s.BaseURL = defaultURL
	}
	return s
}

func (s *Scraper) Do(req *http.Request) (*http.Response, error) {
	for k, v := range defaultHeaders {
		req.Header.Set(k, v)
	}
	res, err := s.Client.Do(req)
	if err != nil {
		return res, err
	}
	if res.StatusCode >= 400 {
		return res, HTTPError{Status: res.Status, StatusCode: res.StatusCode}
	}
	return res, nil
}

func (s *Scraper) Get(url string) (*http.Response, error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	return s.Do(req)
}

func (s *Scraper) Post(url, contentType string, body io.Reader) (*http.Response, error) {
	req, err := http.NewRequest("POST", url, body)
	req.Header.Set("Content-Type", contentType)
	if err != nil {
		return nil, err
	}
	return s.Do(req)
}

func (s *Scraper) PostForm(url string, data url.Values) (resp *http.Response, err error) {
	return s.Post(url, "application/x-www-form-urlencoded", strings.NewReader(data.Encode()))
}
