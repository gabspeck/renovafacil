package clients

import (
	"fmt"
	"time"

	"bitbucket.org/gabspeck/renovafacil/models"
	"sync"
)

const day = 24 * time.Hour

type AuthenticatedClient interface {
	Login() error
	Logout()
}

// WebsiteClient is a client to access insurance company websites
type WebsiteClient interface {
	AuthenticatedClient
	ExportPolicies(from time.Time, until time.Time) ([]byte, error)
	MaxQueryDays() int
	RequestRate() time.Duration
}

// NewClientFunc is a function that returns a WebsiteClient
type NewClientFunc func(params models.ClientParams) WebsiteClient

var clientNewFuncs = map[string]NewClientFunc{}

// DownloadPoliciesByDate downloads policies created between the specified
// dates. The exact meaning of the dates depends on each insurance company.
func DownloadPoliciesByDate(accounts []models.Account, from time.Time, until time.Time, resultCh chan<- *models.QueryResult) {
	wg := &sync.WaitGroup{}
	wg.Add(len(accounts))
	for _, a := range accounts {
		a := a
		go downloadPoliciesFromAccountByDate(&a, resultCh, from, until, wg)
	}
	wg.Wait()
}

func downloadPoliciesFromAccountByDate(account *models.Account, parseCh chan<- *models.QueryResult, from, until time.Time, accountsWg *sync.WaitGroup) {
	defer accountsWg.Done()
	c, err := New(account)
	if err != nil {
		parseCh <- &models.QueryResult{
			Operation: &models.Operation{
				Account: account,
				Err:     err,
			},
		}
		return
	}
	if err := c.Login(); err != nil {
		parseCh <- &models.QueryResult{
			Operation: &models.Operation{
				Account:     account,
				QueryParams: &models.QueryParams{From: from, Until: until},
				Err:         err,
			},
		}
		return
	}
	defer c.Logout()
	params := getSplitQueryParams(from, until, c.MaxQueryDays())
	pagesWg := &sync.WaitGroup{}
	pagesWg.Add(len(params))
	throttle := time.Tick(c.RequestRate())
	for _, p := range params {
		p := p
		if throttle != nil {
			<-throttle
		}
		go func() {
			defer func() {
				pagesWg.Done()
				if err := recover(); err != nil {
					parseCh <- &models.QueryResult{
						Operation: &models.Operation{
							Account:     account,
							QueryParams: &p,
							Err:         err.(error),
						},
					}
				}
			}()
			companyPolicies, err := c.ExportPolicies(p.From, p.Until)
			parseCh <- &models.QueryResult{
				Operation: &models.Operation{
					Account:     account,
					QueryParams: &p,
					Err:         err,
				},
				File: companyPolicies,
			}
		}()
	}
	// Aguardar o fim de todas as requisições para fazer o logout
	pagesWg.Wait()
}

func getSplitQueryParams(from time.Time, until time.Time, daysPerRequest int) (params []models.QueryParams) {
	fromDay, untilDay := getUTCDay(from), getUTCDay(until)
	requestFrom := fromDay
	remainingDays := int(untilDay.Sub(fromDay) / day)
	var requestUntil time.Time
	for remainingDays >= 0 {
		requestDays := min(remainingDays, daysPerRequest)
		requestUntil = requestFrom.AddDate(0, 0, requestDays)
		params = append(params, models.QueryParams{From: requestFrom, Until: requestUntil})
		remainingDays -= requestDays + 1
		requestFrom = requestUntil.AddDate(0, 0, 1)
	}
	return
}

func getUTCDay(in time.Time) time.Time {
	return time.Date(in.Year(), in.Month(), in.Day(), 0, 0, 0, 0, time.UTC)
}

// RegisterNewClientFunc registers a WebsiteClient's instantiation function
func RegisterNewClientFunc(key string, f NewClientFunc) {
	clientNewFuncs[key] = f
}

func GetClientKeys() []string {
	s := make([]string, len(clientNewFuncs))
	var i int
	for k := range clientNewFuncs {
		s[i] = k
		i += 1
	}
	return s
}

func New(account *models.Account) (client WebsiteClient, err error) {
	newFunc, hasCompany := clientNewFuncs[account.Company.Name]
	if !hasCompany {
		err = fmt.Errorf("no client registered for company '%s'", account.Company.Name)
	} else {
		client = newFunc(models.NewClientParamsFromAccount(account))
	}
	return
}

func min(x, y int) int {
	if x < y {
		return x
	}
	return y
}
