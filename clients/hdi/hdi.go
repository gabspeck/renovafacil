package hdi

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"time"

	"bitbucket.org/gabspeck/renovafacil/clients"
	"bitbucket.org/gabspeck/renovafacil/models"
	"github.com/PuerkitoBio/goquery"
)

const (
	urlBase               = "https://www.hdi.com.br"
	pathLogin             = "/digital2/j_spring_security_check"
	pathLogout            = "/digital2/j_spring_security_logout"
	formLegado            = "/digital2/legado/dsp_menu_meu_portal_admin_2008/1/2"
	pathAdminMenu         = "/scripts/cgiip.exe/WService=wsbroker2//hdidigital/dsp_menu_meu_portal_admin_2008.htm"
	pathDspExportaCalculo = "/scripts/cgiip.exe/WService=wsbroker2//hdidigital/dsp_exporta_calculo_2008.htm"
	pathActExportaCalculo = "/scripts/cgiip.exe/WService=wsbroker2//hdidigital/act_exporta_calculo_novo_2008.htm"
	pathEnviarDownload    = "/scripts/cgiip.exe/WService=wsbroker2//hdidigital/enviar_download.htm"
	fsDownloadPath        = "/hdifs/download/arquivo/bloco/"
	maxQueryDays          = 30
)

var ErrHdiBadPeriod = errors.New("hdi: período deve ser entre 1 e 30 dias")
var attemptInterval = 5 * time.Second

type Client struct {
	*clients.Scraper
	chaveUsuario string
}

func init() {
	clients.RegisterNewClientFunc("HDI", NewClient)
}

func NewClient(params models.ClientParams) clients.WebsiteClient {
	c := &Client{
		Scraper: clients.NewScraper(params, urlBase),
	}
	return c
}

func (*Client) MaxQueryDays() int {
	return maxQueryDays
}

func (*Client) RequestRate() time.Duration {
	return time.Second * 10
}

func (h *Client) Get(url string) (*http.Response, error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	if h.chaveUsuario != "" {
		q := req.URL.Query()
		q.Set("chaveUsuario", h.chaveUsuario)
		req.URL.RawQuery = q.Encode()
	}
	return h.Do(req)
}

func tryParseJSONFromResponse(b io.ReadCloser) map[string]interface{} {
	var (
		dec = json.NewDecoder(b)
		m   = map[string]interface{}{}
		err = dec.Decode(&m)
	)
	b.Close()
	if err != nil {
		return nil
	}
	return m
}

func (h *Client) Login() error {
	loginForm := url.Values{
		"j_username": {h.Username},
		"j_password": {h.Password},
	}
	resp, err := h.PostForm(h.BaseURL+pathLogin, loginForm)
	var (
		m       map[string]interface{}
		jsonErr error
	)
	if resp != nil {
		m = tryParseJSONFromResponse(resp.Body)
	}
	if err != nil {
		if m != nil {
			return fmt.Errorf("hdi: erro do servidor: %s", m["c_msgerro"])
		}
		return fmt.Errorf("hdi: erro no post do login: %s", err)
	}
	if jsonErr != nil {
		return fmt.Errorf("hdi: erro ao obter URL para login: %s", err)
	}
	resourceToRedirect := m["resource_to_redirect"].(string)
	parsedUrl, err := url.Parse(resourceToRedirect)
	if err != nil {
		return fmt.Errorf("hdi: erro ao interpretar URL: %s", err)
	}
	if _, err := h.Get(resourceToRedirect); err != nil {
		return fmt.Errorf("hdi: erro ao visitar home: %s", err)
	}
	h.chaveUsuario = parsedUrl.Query().Get("chaveUsuario")
	return nil
}

func (h *Client) Logout() {
	h.Get(h.BaseURL + pathLogout)
	h.chaveUsuario = ""
}

func (h *Client) ExportPolicies(from time.Time, until time.Time) ([]byte, error) {
	if h.chaveUsuario == "" {
		return nil, errors.New("hdi: não autenticado")
	}

	if periodDays := until.Sub(from).Hours() / 24; periodDays < 0 || periodDays > maxQueryDays {
		return nil, ErrHdiBadPeriod
	}

	menuResp, err := h.Get(h.BaseURL + formLegado)
	if err != nil {
		return nil, fmt.Errorf("hdi: erro ao obter página do form legado: %s", err)
	}
	menuDoc, err := goquery.NewDocumentFromResponse(menuResp)
	if err != nil {
		return nil, fmt.Errorf("hdi: erro no parse do menu: %s", err)
	}
	formLegadoValues := parseHTMLForm(menuDoc.Selection, "#formCarregarLegado")
	if len(formLegadoValues) < 1 {
		return nil, fmt.Errorf("hdi: formCarregarLegado não encontrado")
	}
	adminMenuResp, err := h.PostForm(h.BaseURL+pathAdminMenu, formLegadoValues)
	if err != nil {
		return nil, fmt.Errorf("hdi: erro ao buscar menu admin: %s", err)
	}
	adminMenuDoc, err := goquery.NewDocumentFromResponse(adminMenuResp)
	if err != nil {
		return nil, fmt.Errorf("hdi: erro ao interpretar menu admin: %s", err)
	}
	ajaxParams := url.Values{}
	adminMenuDoc.Find("input").Each(func(i int, e *goquery.Selection) {
		k, ok := e.Attr("name")
		if !ok {
			return
		}
		switch k {
		case "m_cod_empresa_session",
			"m_cod_sucursal_session",
			"m_nm_dbname",
			"t_prd",
			"c_pc",
			"t_prd_grupo",
			"c_pc_grupo",
			"m_flg_intranet",
			"m_c_sucursais",
			"m_n_sucursais",
			"m_c_sucs_grupo",
			"m_server_topo",
			"m_broker_topo",
			"m_n_sucs_grupo":
			ajaxParams.Set(k, e.AttrOr("value", ""))
		case "m_cpf_prdtor":
			{
				v := e.AttrOr("value", "")
				ajaxParams.Set(k, v)
				ajaxParams.Set("c_cpf_prdtor", v)
			}

		}
	})
	ajaxParams.Set("versao_login", "")
	goquery.NewDocumentFromResponse(adminMenuResp)

	dspExportResp, err := h.PostForm(h.BaseURL+pathDspExportaCalculo, ajaxParams)
	if err != nil {
		return nil, fmt.Errorf("hdi: erro ao obter formulário de exportação: %s", err)
	}
	dspExportDoc, err := goquery.NewDocumentFromResponse(dspExportResp)
	if err != nil {
		return nil, fmt.Errorf("hdi: erro no parse da página de exportação: %s", err)
	}
	fEntrada := parseHTMLForm(dspExportDoc.Selection, "#f_entrada")
	if len(fEntrada) < 1 {
		return nil, fmt.Errorf("hdi: página de exportação inválida")
	}
	actUrl, _ := url.Parse(h.BaseURL + pathActExportaCalculo)
	actQry := actUrl.Query()
	for k, v := range fEntrada {
		for _, s := range v {
			actQry.Add(k, s)
		}
	}
	actQry.Set("m_dat_inicial", from.Format("02/01/2006"))
	actQry.Set("m_dat_final", until.Format("02/01/2006"))
	actQry.Set("m_situacao", "A")
	actUrl.RawQuery = actQry.Encode()

	const (
		maxAttempts = 6
	)
	var (
		scriptText     string
		actDoc         *goquery.Document
		attempts       = 0
		fEntradaValues = url.Values{}
	)
	for k, v := range ajaxParams {
		fEntradaValues[k] = v
	}
	for attempts < maxAttempts {
		if attempts == 1 {
			actUrl.RawQuery += "&" + ajaxParams.Encode()
		}
		actResp, err := h.PostForm(actUrl.String(), fEntradaValues)
		if err != nil {
			return nil, fmt.Errorf("hdi: erro ao enviar formulário de exportação: %s", err)
		}
		actDoc, err = goquery.NewDocumentFromResponse(actResp)
		if err != nil {
			return nil, fmt.Errorf("hdi: erro no parse da resposta do formulário de exportação: %s", err)
		}
		scriptText = actDoc.Find("script").Text()
		if strings.Contains(scriptText, "document.f_download.submit();") {
			break
		}
		fEntradaValues = parseHTMLForm(actDoc.Selection, "#f_entrada")
		attempts++
		time.Sleep(attemptInterval)
	}
	if attempts >= maxAttempts {
		return nil, errors.New("hdi: número de tentativas esgotado para obter arquivo de exportação")
	}
	fDownloadValues := parseHTMLForm(actDoc.Selection, `form[name="f_download"]`)
	enviarDownloadResp, err := h.PostForm(h.BaseURL+pathEnviarDownload, fDownloadValues)
	if err != nil {
		return nil, fmt.Errorf("hdi: erro ao enviar formulário enviar_download: %s", err)
	}
	enviarDownloadDoc, err := goquery.NewDocumentFromResponse(enviarDownloadResp)
	if err != nil {
		return nil, fmt.Errorf("hdi: erro ao interpretar resposta do enviar_download: %s", err)
	}
	enviarDownloadForm := enviarDownloadDoc.Find("form")
	enviarDownloadValues := parseHTMLForm(enviarDownloadForm, "")
	phpPostUrl, _ := enviarDownloadForm.Attr("action")
	phpResp, err := h.PostForm(phpPostUrl, enviarDownloadValues)
	if err != nil {
		return nil, fmt.Errorf("hdi: erro ao enviar POST ao PHP: %s", err)
	}
	phpDoc, err := goquery.NewDocumentFromResponse(phpResp)
	if err != nil {
		return nil, fmt.Errorf("hdi: erro ao interpretar resposta do PHP: %s", err)
	}
	hdiFsScript := phpDoc.Find("script").Text()
	reHdiFsLink := regexp.MustCompile(`/hdifs/download/bloco/(.+)"`)
	downloadId := reHdiFsLink.FindStringSubmatch(hdiFsScript)[1]
	hdiFsUrl := h.BaseURL + fsDownloadPath + downloadId
	polResp, err := h.Get(hdiFsUrl)
	if err != nil {
		return nil, fmt.Errorf("hdi: erro ao baixar arquivo de apólices: %s", err)
	}
	defer polResp.Body.Close()
	respBytes, err := ioutil.ReadAll(polResp.Body)
	if err != nil {
		return nil, fmt.Errorf("hdi: erro ao obter bytes do arquivo de apólices: %s", err)
	}
	return respBytes, nil

}

func parseHTMLForm(d *goquery.Selection, sel string) url.Values {
	elSel := sel + " input"
	v := url.Values{}
	d.Find(elSel).Each(func(_ int, e *goquery.Selection) {
		k, _ := e.Attr("name")
		if k == "" {
			return
		}
		t := e.AttrOr("type", "")
		if t == "radio" || t == "checkbox" {
			_, checked := e.Attr("checked")
			if !checked {
				return
			}
		}
		v.Add(k, e.AttrOr("value", ""))
	})
	return v
}
