package hdi

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"net/url"
	"sort"
	"testing"
	"text/template"
	"time"

	"bitbucket.org/gabspeck/renovafacil/models"
)

func assertFormContainsExpectedKeysAndValues(expected url.Values, actual url.Values) (ok bool) {
	ok = true
	if lA, lE := len(actual), len(expected); lA < lE {
		log.Printf("wrong length. wanted %d; got %d\n", lE, lA)
		ok = false
	}
	for eK, eV := range expected {
		if aV, present := actual[eK]; !present {
			ok = false
			log.Printf("key %s missing from map\n", eK)
		} else {
			sort.Strings(eV)
			sort.Strings(aV)
			if seV, saV := fmt.Sprint(eV), fmt.Sprint(aV); seV != saV {
				ok = false
				log.Printf("values for key %s differ. wanted %s, got %s", eK, eV, aV)
			}
		}
	}
	return
}

func newHdiServer() *httptest.Server {

	templateParams := map[string]string{}
	templates := template.Must(template.ParseGlob("testdata/*.html"))

	var loginHandler = func(w http.ResponseWriter, r *http.Request) {
		enc := json.NewEncoder(w)
		r.ParseForm()
		if login, pass := r.PostForm.Get("j_username"), r.PostForm.Get("j_password"); login != "aladdin" || pass != "opensesame" {
			loginErr := map[string]interface{}{
				"l_ok":      false,
				"c_msgerro": "Login e ou senha inválido(s)",
			}
			w.WriteHeader(http.StatusForbidden)
			w.Header().Set("Content-Type", "application/json;charset=UTF-8")
			enc.Encode(&loginErr)
			return
		}
		enc.Encode(map[string]string{"resource_to_redirect": fmt.Sprintf("http://%s/digital2/home?chaveUsuario=ABC123DEF", r.Host)})
	}

	var hdiReturnInvalidAccess = func(w http.ResponseWriter) {
		templates.ExecuteTemplate(w, "admin_invalid_access.html", nil)
		return
	}

	var homeHandler = func(w http.ResponseWriter, r *http.Request) {
		if chaveUsuario := r.URL.Query().Get("chaveUsuario"); chaveUsuario == "" {
			log.Println("chaveUsuario empty")
			w.WriteHeader(http.StatusBadRequest)
			templates.ExecuteTemplate(w, "home_error_no_chaveusuario.html", templateParams)
			return
		} else if chaveUsuario != "ABC123DEF" {
			log.Printf("wrong chaveUsuario. wanted 'ABC123DEF', got '%s'.\n", chaveUsuario)
			templates.ExecuteTemplate(w, "home_error_wrong_chaveusuario.html", templateParams)
			return
		}
		http.SetCookie(w, &http.Cookie{
			Name:  "homeCookie",
			Value: "123-abc-456-def",
		})
		templates.ExecuteTemplate(w, "home.html", templateParams)
	}

	var adminHandler = func(w http.ResponseWriter, r *http.Request) {
		var chaveUsuario, accept string
		if chaveUsuario, accept = r.URL.Query().Get("chaveUsuario"),
			r.Header.Get("Accept"); chaveUsuario == "ABC123DEF" && accept != "" {
			if c, _ := r.Cookie("homeCookie"); c != nil && c.Value == "123-abc-456-def" {
				templates.ExecuteTemplate(w, "admin_form.html", templateParams)
				return
			}
			log.Println("bad cookie. have you been to the home page?")
			templates.ExecuteTemplate(w, "admin_form_nocookie.html", templateParams)
			return
		}
		log.Printf("chaveUsuario: %s; Accept: %s", chaveUsuario, accept)
		http.Error(w, "Server error", http.StatusInternalServerError)
	}
	var adminMenuHandler = func(w http.ResponseWriter, r *http.Request) {
		expectedForm := url.Values{
			"c_pc_grupo":            {"N123456789012_0123"},
			"m_cod_corretor":        {"100123456"},
			"l_hsbc":                {"false"},
			"m_c_sucs_grupo":        {"098"},
			"m_flg_admin":           {"true"},
			"initparam":             {"48aa27aa2968989849276898ee21xx29aa7829ee61byfteujpdveucxbyazazeuftaacxcxcxazxx7899698958994768382799ee7927277817eebbbb979797yy793989yy296848yy1998bb39896989279938cxbb79684849bb476838279998"},
			"init":                  {"21xxbyazazeuftcxjpbycxxxazbyiqxxxxazjpeuiqdvhrbydvjpazazxx7957dvdv2728jpgsxxbyazazeuftcxjpbycxxx21"},
			"m_cpf_prdtor":          {"01234444876"},
			"m_c_sucursais":         {"098"},
			"m_site_2008":           {"true"},
			"m_flg_intranet":        {"FALSE"},
			"m_cod_opcao_menu_2008": {"2"},
			"m_cod_sucursal":        {"098"},
			"c_pc":                  {"A123456789012_0123"},
			"m_t_corretor":          {"C"},
			"m_cod_empresa":         {"01"},
			"c_pc_orig":             {"A123456789012_0123"},
			"m_nome_user_web":       {"01234444876"},
			"chaveUsuario":          {"ABC123DEF"},
			"m_n_sucs_grupo":        {"JOINVILLE"},
			"m_sta_origem":          {"H"},
			"m_n_sucursais":         {"JOINVILLE"},
			"t_prd_grupo":           {"C"},
			"IFrameRequest":         {"true"},
			"m_user_id":             {""},
			"m_cod_suc_intranet":    {""},
			"m_frame_hdidigital":    {"1"},
		}
		r.ParseForm()
		if !assertFormContainsExpectedKeysAndValues(expectedForm, r.PostForm) {
			log.Println("adminMenuHandler: bad form")
			hdiReturnInvalidAccess(w)
			return
		}
		templates.ExecuteTemplate(w, "dsp_menu_meu_portal_admin_2008.html", templateParams)
		return

	}

	var adminExportPoliciesHandler = func(w http.ResponseWriter, r *http.Request) {
		expectedForm, _ := url.ParseQuery(
			"c_pc_grupo=N123456789012_0123&" +
				"t_prd_grupo=C&")
		r.ParseForm()
		if !assertFormContainsExpectedKeysAndValues(expectedForm, r.PostForm) {
			log.Println("adminExportPoliciesHandler: bad form")
			hdiReturnInvalidAccess(w)
			return
		}
		w.Header().Set("Content-Type", "text/html; charset=iso8859-1")
		templates.ExecuteTemplate(w, "dsp_exporta_calculo_2008.html", templateParams)
	}

	var adminExportPoliciesActHandler = func() http.HandlerFunc {
		waitRepeatCount := 0
		return func(w http.ResponseWriter, r *http.Request) {
			expectedQueryString, _ := url.ParseQuery(
				"t_produtor=C&" +
					"cod_produtor=100123456&" +
					"cpf_prdtor=01234444876&" +
					"m_situacao=A&" +
					"m_layout=4&")
			secondExpectedQueryString, _ := url.ParseQuery(
				expectedQueryString.Encode() +
					"&m_cod_empresa_session=01&" +
					"m_cod_sucursal_session=098&" +
					"m_nm_dbname=&" +
					"t_prd=C&" +
					"c_pc=A123456789012_0123&" +
					"t_prd_grupo=C&" +
					"c_pc_grupo=N123456789012_0123&" +
					"m_flg_intranet=FALSE&" +
					"m_c_sucursais=098&" +
					"m_n_sucursais=JOINVILLE&" +
					"m_c_sucs_grupo=098&" +
					"m_server_topo=&" +
					"m_broker_topo=&" +
					"m_n_sucs_grupo=JOINVILLE")
			expectedForm, _ := url.ParseQuery(
				"m_cod_empresa_session=01&" +
					"m_cod_sucursal_session=098&" +
					"m_nm_dbname=&" +
					"t_prd=C&" +
					"c_pc=A123456789012_0123&" +
					"t_prd_grupo=C&" +
					"c_pc_grupo=N123456789012_0123&" +
					"c_cpf_prdtor=01234444876&" +
					"m_cpf_prdtor=01234444876&" +
					"m_flg_intranet=FALSE&" +
					"m_c_sucursais=098&" +
					"m_n_sucursais=JOINVILLE&" +
					"m_c_sucs_grupo=098&" +
					"m_n_sucs_grupo=JOINVILLE&" + "" +
					"m_server_topo=&" +
					"m_broker_topo=&" +
					"versao_login=",
			)
			expectedFEntrada, _ := url.ParseQuery(
				"m_status_prod=&" +
					"m_sta_origem=&" +
					"m_t_corretor=&" +
					"m_cod_corretor=&" +
					"c_pc=C173521004549_2320&" +
					"c_prdtor=&" +
					"m_c_sucursais=018&" +
					"m_cod_sucursal=&" +
					"t_produtor=C&" +
					"cod_produtor=100123456&" +
					"m_situacao=A&" +
					"m_layout=4&" +
					"t_prd_grupo=C&" +
					"c_pc_grupo=R185210045493_2320&" +
					"m_num_requisicao=13558804&" +
					"m_control_voltar=")
			r.ParseForm()
			if !assertFormContainsExpectedKeysAndValues(expectedQueryString, r.URL.Query()) {
				log.Println("adminExportPoliciesActionHandler: bad query string")
				hdiReturnInvalidAccess(w)
				return
			}
			if waitRepeatCount < 3 {
				w.Header().Set("Content-Type", "text/html;charset=iso8859-1")
				var formToValidate url.Values
				var qsToValidate url.Values
				if waitRepeatCount == 0 {
					formToValidate = expectedForm
					qsToValidate = expectedQueryString
				} else {
					formToValidate = expectedFEntrada
					qsToValidate = secondExpectedQueryString
				}
				const (
					startDateField = "m_dat_inicial"
					endDateField   = "m_dat_final"
				)

				badDates := qsToValidate.Get(startDateField) == "" || qsToValidate.Get(endDateField) == "" ||
					formToValidate.Get(startDateField) == "" || formToValidate.Get(endDateField) == ""

				if !badDates || !assertFormContainsExpectedKeysAndValues(qsToValidate, r.URL.Query()) {
					if waitRepeatCount == 0 {
						hdiReturnInvalidAccess(w)
						return
					}
					if waitRepeatCount == 1 {
						templates.ExecuteTemplate(w, "act_exporta_calculo_novo_2008_req_nr_not_found.html", templateParams)
						return
					}
					waitRepeatCount = 1
				}
				if assertFormContainsExpectedKeysAndValues(formToValidate, r.PostForm) {
					templates.ExecuteTemplate(w, "act_exporta_calculo_novo_2008_wait.html", templateParams)
					waitRepeatCount++
				} else {
					if waitRepeatCount == 0 {
						hdiReturnInvalidAccess(w)
						return
					} else {
						templates.ExecuteTemplate(w, "act_exporta_calculo_novo_2008_wait.html", templateParams)
					}
					log.Println("adminExportPoliciesActionHandler: bad f_entrada")
				}
				return
			}
			w.Header().Set("Content-Type", "text/html;charset=iso8859-1")
			templates.ExecuteTemplate(w, "act_exporta_calculo_novo_2008_done.html", templateParams)
			waitRepeatCount = 0
			return
		}

	}

	var enviarDownloadHandler = func(w http.ResponseWriter, r *http.Request) {
		expectedForm, _ := url.ParseQuery("m_caminho=1&m_param=exporta_calculo%2FC100123456_apolices_emitidas.txt")
		r.ParseForm()
		if expected, actual := expectedForm.Encode(), r.PostForm.Encode(); expected != actual {
			templates.ExecuteTemplate(w, "enviar_download_no_filename.htm", templateParams)
			return
		}
		templates.ExecuteTemplate(w, "enviar_download.html", templateParams)
		return
	}

	var enviarDownloadPhpHandler = func(w http.ResponseWriter, r *http.Request) {
		expectedForm, _ := url.ParseQuery(
			"caminho=101&" +
				"param=%2BeZAzA9KEgYDNRF1kvazog%3D%3D%7C48%2Fo3bz49dIpd4wck9sHXwpaudO8X4UuhznZVTk7RQ5YhV9Rjrb5ctua%2FJScvC2En7TK7qwey%2Fm3vB5XsZgUmg%3D%3D")
		r.ParseForm()
		if !assertFormContainsExpectedKeysAndValues(expectedForm, r.PostForm) {
			log.Println("enviarDownloadPhpHandler: bad form")
			templates.ExecuteTemplate(w, "enviar_download.php_no_filename.html", templateParams)
			return
		}
		templates.ExecuteTemplate(w, "enviar_download.php.html", templateParams)
		return
	}

	var fsDownloadHandler = func(w http.ResponseWriter, r *http.Request) {
		pg, _ := ioutil.ReadFile("testdata/C100123456_apolices_emitidas_1491175535610.txt")
		w.Header().Set("Content-Type", "application/octet-stream")
		w.Header().Set("Content-Disposition", `attachment; filename="clientestdata/C100123456_apolices_emitidas_1491175535610.txt"`)
		w.Write(pg)
		return
	}

	var handlers = map[string]http.HandlerFunc{
		"/digital2/home":                                                                       homeHandler,
		"/digital2/j_spring_security_check":                                                    loginHandler,
		"/digital2/legado/dsp_menu_meu_portal_admin_2008/1/2":                                  adminHandler,
		"/scripts/cgiip.exe/WService=wsbroker2//hdidigital/dsp_menu_meu_portal_admin_2008.htm": adminMenuHandler,
		"/scripts/cgiip.exe/WService=wsbroker2//hdidigital/dsp_exporta_calculo_2008.htm":       adminExportPoliciesHandler,
		"/scripts/cgiip.exe/WService=wsbroker2//hdidigital/act_exporta_calculo_novo_2008.htm":  adminExportPoliciesActHandler(),
		"/scripts/cgiip.exe/WService=wsbroker2//hdidigital/enviar_download.htm":                enviarDownloadHandler,
		"/segurado/enviar_download.php":                                                        enviarDownloadPhpHandler,
		"/hdifs/download/arquivo/bloco/CYzasJj4hfuAAzVkRT7rpUfT0l7ZH7SC":                       fsDownloadHandler,
	}

	s := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		h := handlers[r.URL.Path]
		if h == nil {
			http.NotFound(w, r)
			return
		}
		h(w, r)
	}))
	templateParams["HdiURL"] = s.URL
	return s
}

func TestHdiClientLogin(t *testing.T) {
	type loginTestParam struct {
		params models.ClientParams
		err    error
	}

	s := newHdiServer()
	defer s.Close()
	tests := map[string]loginTestParam{
		"correct username and password": {
			params: models.ClientParams{"username": "aladdin", "password": "opensesame", "base_url": s.URL},
			err:    nil,
		},
		"bad username": {
			params: models.ClientParams{"username": "abacaxi", "password": "opensesame", "base_url": s.URL},
			err:    errors.New("hdi: erro do servidor: Login e ou senha inválido(s)"),
		},
	}
	for name, params := range tests {
		c := NewClient(params.params)
		if actual, expected := fmt.Sprint(c.Login()), fmt.Sprint(params.err); expected != actual {
			t.Errorf("%s:\nwanted:\n %s\ngot:\n%s", name, expected, actual)
		}
	}
}

func TestHdiClientExportPolicies(t *testing.T) {
	oldInterval := attemptInterval
	defer func() {
		attemptInterval = oldInterval
	}()
	attemptInterval = 0
	s := newHdiServer()
	defer s.Close()

	type test struct {
		startDate    time.Time
		endDate      time.Time
		responseFile string
		err          error
	}

	tests := map[string]test{
		"valid request": {
			startDate:    time.Date(2017, 3, 1, 0, 0, 0, 0, time.Local),
			endDate:      time.Date(2017, 3, 8, 0, 0, 0, 0, time.Local),
			responseFile: "testdata/C100123456_apolices_emitidas_1491175535610.txt",
			err:          nil,
		},
		"start before end": {
			startDate:    time.Date(2017, 3, 8, 0, 0, 0, 0, time.Local),
			endDate:      time.Date(2017, 3, 1, 0, 0, 0, 0, time.Local),
			responseFile: "",
			err:          ErrHdiBadPeriod,
		},
		"same day": {
			startDate:    time.Date(2017, 3, 8, 0, 0, 0, 0, time.Local),
			endDate:      time.Date(2017, 3, 8, 0, 0, 0, 0, time.Local),
			responseFile: "testdata/C100123456_apolices_emitidas_1491175535610.txt",
			err:          nil,
		},
		"30 days": {
			startDate:    time.Date(2017, 3, 1, 0, 0, 0, 0, time.Local),
			endDate:      time.Date(2017, 3, 31, 0, 0, 0, 0, time.Local),
			responseFile: "testdata/C100123456_apolices_emitidas_1491175535610.txt",
			err:          nil,
		},
		"31 days": {
			startDate:    time.Date(2017, 3, 1, 0, 0, 0, 0, time.Local),
			endDate:      time.Date(2017, 4, 1, 0, 0, 0, 0, time.Local),
			responseFile: "",
			err:          ErrHdiBadPeriod,
		},
	}

	hdiParams := models.ClientParams{"username": "aladdin", "password": "opensesame", "base_url": s.URL}
	c := NewClient(hdiParams)
	for k, tst := range tests {
		func() {
			c.Login()
			defer c.Logout()
			var (
				expectedFile []byte = nil
				err          error
			)
			if tst.responseFile != "" {
				expectedFile, err = ioutil.ReadFile(tst.responseFile)
				if err != nil {
					t.Errorf("%s: error opening reference file: %s", k, err)
					return
				}
			}
			pf, err := c.ExportPolicies(tst.startDate, tst.endDate)
			if fmt.Sprint(err) != fmt.Sprint(tst.err) {
				t.Errorf("%s: wanted error %s, got %s", k, tst.err, err)
			}
			if bytes.Compare(expectedFile, pf) != 0 {
				t.Errorf("%s: wanted file\n%s\ngot file\n%s", k, string(expectedFile), string(pf))
			}

		}()
	}

}
