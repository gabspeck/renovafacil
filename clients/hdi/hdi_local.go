package hdi

import (
	"bytes"
	"errors"
	"io/ioutil"
	"math"
	"os"
	"path/filepath"
	"strings"
	"time"

	"bitbucket.org/gabspeck/renovafacil/clients"
	"bitbucket.org/gabspeck/renovafacil/models"
)

type LocalClient struct {
	path string
}

func (*LocalClient) Login() error { return nil }
func (*LocalClient) Logout()      {}
func (c *LocalClient) ExportPolicies(from, until time.Time) ([]byte, error) {

	finalFile := bytes.Buffer{}
	err := filepath.Walk(c.path, func(p string, i os.FileInfo, err error) error {
		if !i.IsDir() && strings.HasSuffix(p, ".txt") {
			thisFile, err := ioutil.ReadFile(p)
			if err != nil {
				return err
			}
			_, err = finalFile.Write(thisFile)
			if err != nil {
				return err
			}
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	b := finalFile.Bytes()
	if len(b) < 1 {
		return nil, errors.New("no content")
	}
	return b, nil
}
func (*LocalClient) MaxQueryDays() int {
	return math.MaxInt32
}
func (*LocalClient) RequestRate() time.Duration {
	return 0
}

func NewLocalClient(p models.ClientParams) clients.WebsiteClient {
	return &LocalClient{
		path: p["path"],
	}
}
