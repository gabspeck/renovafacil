package clients

import (
	"errors"
	"fmt"
	"testing"
	"time"

	"bitbucket.org/gabspeck/renovafacil/models"
	"github.com/kylelemons/godebug/pretty"
	"sort"
	"runtime/debug"
)

type mock30DayQueryClient struct {
	loggedIn bool
}
type mockPageErrorClient struct {
	loggedIn bool
}

func TestDownloadPoliciesByDate(t *testing.T) {
	oldClients := clientNewFuncs
	defer func() {
		clientNewFuncs = oldClients
	}()

	clientNewFuncs = map[string]NewClientFunc{
		"mock":      newMock30DayQueryClient,
		"pageerror": newMockPageErrorClient,
	}

	var (
		tzSaoPaulo *time.Location
		err        error
	)
	if tzSaoPaulo, err = time.LoadLocation("America/Sao_Paulo"); err != nil {
		t.Fatal(err)
	}
	mockAccount := &models.Account{
		Name: "mock",
		Company: &models.Company{
			Name: "mock",
		},
	}
	pageerrorAccount := &models.Account{
		Name: "pageerror",
		Company: &models.Company{
			Name: "pageerror",
		},
	}

	type test struct {
		client  WebsiteClient
		account *models.Account
		from    time.Time
		until   time.Time
		qr      models.QueryResults
	}
	tests := map[string]test{
		"one day": {
			client:  newMock30DayQueryClient(nil),
			account: mockAccount,
			from:    time.Date(2017, 3, 1, 0, 0, 0, 0, time.UTC),
			until:   time.Date(2017, 3, 1, 0, 0, 0, 0, time.UTC),
			qr: models.QueryResults{
				{
					Operation: &models.Operation{
						Account: mockAccount,
						QueryParams: &models.QueryParams{From: time.Date(2017, 3, 1, 0, 0, 0, 0, time.UTC),
							Until: time.Date(2017, 3, 1, 0, 0, 0, 0, time.UTC)},
					},
					File: []byte("from: 01/03/2017 until: 01/03/2017"),
				},
			},
		},
		"period greater than client's maximum": {
			client:  newMock30DayQueryClient(nil),
			account: mockAccount,
			from:    time.Date(2017, time.March, 1, 0, 0, 0, 0, time.UTC),
			until:   time.Date(2017, time.June, 15, 0, 0, 0, 0, time.UTC),
			qr: models.QueryResults{
				{
					Operation: &models.Operation{
						Account: mockAccount,
						QueryParams: &models.QueryParams{
							From:  time.Date(2017, time.March, 1, 0, 0, 0, 0, time.UTC),
							Until: time.Date(2017, time.March, 31, 0, 0, 0, 0, time.UTC),
						},
					},
					File: []byte("from: 01/03/2017 until: 31/03/2017"),
				}, {
					Operation: &models.Operation{
						Account: mockAccount,
						QueryParams: &models.QueryParams{
							From:  time.Date(2017, time.April, 1, 0, 0, 0, 0, time.UTC),
							Until: time.Date(2017, time.May, 01, 0, 0, 0, 0, time.UTC),
						},
					},
					File: []byte("from: 01/04/2017 until: 01/05/2017"),
				},
				{
					Operation: &models.Operation{
						Account: mockAccount,
						QueryParams: &models.QueryParams{
							From:  time.Date(2017, time.May, 2, 0, 0, 0, 0, time.UTC),
							Until: time.Date(2017, time.June, 1, 0, 0, 0, 0, time.UTC),
						},
					},
					File: []byte("from: 02/05/2017 until: 01/06/2017"),
				},
				{
					Operation: &models.Operation{
						Account: mockAccount,
						QueryParams: &models.QueryParams{
							From:  time.Date(2017, time.June, 2, 0, 0, 0, 0, time.UTC),
							Until: time.Date(2017, time.June, 15, 0, 0, 0, 0, time.UTC),
						},
					},
					File: []byte("from: 02/06/2017 until: 15/06/2017"),
				},
			},
		},
		"page download error": {
			client:  newMockPageErrorClient(nil),
			account: pageerrorAccount,
			from:    time.Date(2017, time.March, 1, 0, 0, 0, 0, time.UTC),
			until:   time.Date(2017, time.June, 15, 0, 0, 0, 0, time.UTC),
			qr: models.QueryResults{
				{
					Operation: &models.Operation{
						Account: pageerrorAccount,
						QueryParams: &models.QueryParams{
							From:  time.Date(2017, time.March, 1, 0, 0, 0, 0, time.UTC),
							Until: time.Date(2017, time.March, 31, 0, 0, 0, 0, time.UTC),
						},
						Err: errors.New("odd months are not supported"),
					},
					File: nil,
				},
				{Operation: &models.Operation{Account: pageerrorAccount,
					QueryParams: &models.QueryParams{
						From:  time.Date(2017, time.April, 1, 0, 0, 0, 0, time.UTC),
						Until: time.Date(2017, time.May, 1, 0, 0, 0, 0, time.UTC),
					},
					Err: nil,
				},
					File: []byte("from: 01/04/2017 until: 01/05/2017"),
				},
				{
					Operation: &models.Operation{
						Account: pageerrorAccount,
						QueryParams: &models.QueryParams{
							From:  time.Date(2017, time.May, 2, 0, 0, 0, 0, time.UTC),
							Until: time.Date(2017, time.June, 1, 0, 0, 0, 0, time.UTC),
						},
						Err: errors.New("odd months are not supported"),
					},
					File: nil,
				},
				{
					Operation: &models.Operation{
						Account: pageerrorAccount,
						QueryParams: &models.QueryParams{
							From:  time.Date(2017, time.June, 2, 0, 0, 0, 0, time.UTC),
							Until: time.Date(2017, 6, 15, 0, 0, 0, 0, time.UTC),
						},
						Err: nil,
					},
					File: []byte("from: 02/06/2017 until: 15/06/2017"),
				},
			},
		},
		"one year ago": {
			client:  newMock30DayQueryClient(nil),
			account: mockAccount,
			from:    time.Date(2016, time.May, 23, 0, 0, 0, 0, time.UTC),
			until:   time.Date(2017, time.May, 23, 0, 0, 0, 0, time.UTC),
			qr: models.QueryResults{
				{
					Operation: &models.Operation{
						Account: mockAccount,
						QueryParams: &models.QueryParams{
							From:  time.Date(2016, time.May, 23, 0, 0, 0, 0, time.UTC),
							Until: time.Date(2016, time.June, 22, 0, 0, 0, 0, time.UTC),
						},
						Err: nil,
					},
					File: []byte("from: 23/05/2016 until: 22/06/2016"),
				},
				{
					Operation: &models.Operation{
						Account: mockAccount,
						QueryParams: &models.QueryParams{
							From:  time.Date(2016, time.June, 23, 0, 0, 0, 0, time.UTC),
							Until: time.Date(2016, time.July, 23, 0, 0, 0, 0, time.UTC),
						},
						Err: nil,
					},
					File: []byte("from: 23/06/2016 until: 23/07/2016"),
				},
				{
					Operation: &models.Operation{
						Account: mockAccount,
						QueryParams: &models.QueryParams{
							From:  time.Date(2016, time.July, 24, 0, 0, 0, 0, time.UTC),
							Until: time.Date(2016, time.August, 23, 0, 0, 0, 0, time.UTC),
						},
						Err: nil,
					},
					File: []byte("from: 24/07/2016 until: 23/08/2016"),
				},
				{
					Operation: &models.Operation{
						Account: mockAccount,
						QueryParams: &models.QueryParams{
							From:  time.Date(2016, time.August, 24, 0, 0, 0, 0, time.UTC),
							Until: time.Date(2016, time.September, 23, 0, 0, 0, 0, time.UTC),
						},
						Err: nil,
					},
					File: []byte("from: 24/08/2016 until: 23/09/2016"),
				},
				{
					Operation: &models.Operation{
						Account: mockAccount,
						QueryParams: &models.QueryParams{
							From:  time.Date(2016, time.September, 24, 0, 0, 0, 0, time.UTC),
							Until: time.Date(2016, time.October, 24, 0, 0, 0, 0, time.UTC),
						},
						Err: nil,
					},
					File: []byte("from: 24/09/2016 until: 24/10/2016"),
				},
				{
					Operation: &models.Operation{
						Account: mockAccount,
						QueryParams: &models.QueryParams{
							From:  time.Date(2016, time.October, 25, 0, 0, 0, 0, time.UTC),
							Until: time.Date(2016, time.November, 24, 0, 0, 0, 0, time.UTC),
						},
						Err: nil,
					},
					File: []byte("from: 25/10/2016 until: 24/11/2016"),
				},
				{
					Operation: &models.Operation{
						Account: mockAccount,
						QueryParams: &models.QueryParams{
							From:  time.Date(2016, time.November, 25, 0, 0, 0, 0, time.UTC),
							Until: time.Date(2016, time.December, 25, 0, 0, 0, 0, time.UTC),
						},
						Err: nil,
					},
					File: []byte("from: 25/11/2016 until: 25/12/2016"),
				},
				{
					Operation: &models.Operation{
						Account: mockAccount,
						QueryParams: &models.QueryParams{
							From:  time.Date(2016, time.December, 26, 0, 0, 0, 0, time.UTC),
							Until: time.Date(2017, time.January, 25, 0, 0, 0, 0, time.UTC),
						},
						Err: nil,
					},
					File: []byte("from: 26/12/2016 until: 25/01/2017"),
				},
				{
					Operation: &models.Operation{
						Account: mockAccount,
						QueryParams: &models.QueryParams{
							From:  time.Date(2017, time.January, 26, 0, 0, 0, 0, time.UTC),
							Until: time.Date(2017, time.February, 25, 0, 0, 0, 0, time.UTC),
						},
						Err: nil,
					},
					File: []byte("from: 26/01/2017 until: 25/02/2017"),
				},
				{
					Operation: &models.Operation{
						Account: mockAccount,
						QueryParams: &models.QueryParams{
							From:  time.Date(2017, time.February, 26, 0, 0, 0, 0, time.UTC),
							Until: time.Date(2017, time.March, 28, 0, 0, 0, 0, time.UTC),
						},
						Err: nil,
					},
					File: []byte("from: 26/02/2017 until: 28/03/2017"),
				},
				{
					Operation: &models.Operation{
						Account: mockAccount,
						QueryParams: &models.QueryParams{
							From:  time.Date(2017, time.March, 29, 0, 0, 0, 0, time.UTC),
							Until: time.Date(2017, time.April, 28, 0, 0, 0, 0, time.UTC),
						},
						Err: nil,
					},
					File: []byte("from: 29/03/2017 until: 28/04/2017"),
				},
				{
					Operation: &models.Operation{
						Account: mockAccount,
						QueryParams: &models.QueryParams{
							From:  time.Date(2017, time.April, 29, 0, 0, 0, 0, time.UTC),
							Until: time.Date(2017, time.May, 23, 0, 0, 0, 0, time.UTC),
						},
						Err: nil,
					},
					File: []byte("from: 29/04/2017 until: 23/05/2017"),
				},
			},
		},
		"one year ago, non-UTC": {
			client:  newMock30DayQueryClient(nil),
			account: mockAccount,
			from:    time.Date(2016, time.May, 23, 0, 0, 0, 0, tzSaoPaulo),
			until:   time.Date(2017, time.May, 23, 0, 0, 0, 0, tzSaoPaulo),
			qr: models.QueryResults{
				{
					Operation: &models.Operation{
						Account: mockAccount,
						QueryParams: &models.QueryParams{
							From:  time.Date(2016, time.May, 23, 0, 0, 0, 0, time.UTC),
							Until: time.Date(2016, time.June, 22, 0, 0, 0, 0, time.UTC),
						},
						Err: nil,
					},
					File: []byte("from: 23/05/2016 until: 22/06/2016"),
				},
				{
					Operation: &models.Operation{
						Account: mockAccount,
						QueryParams: &models.QueryParams{
							From:  time.Date(2016, time.June, 23, 0, 0, 0, 0, time.UTC),
							Until: time.Date(2016, time.July, 23, 0, 0, 0, 0, time.UTC),
						},
						Err: nil,
					},
					File: []byte("from: 23/06/2016 until: 23/07/2016"),
				},
				{
					Operation: &models.Operation{
						Account: mockAccount,
						QueryParams: &models.QueryParams{
							From:  time.Date(2016, time.July, 24, 0, 0, 0, 0, time.UTC),
							Until: time.Date(2016, time.August, 23, 0, 0, 0, 0, time.UTC),
						},
						Err: nil,
					},
					File: []byte("from: 24/07/2016 until: 23/08/2016"),
				},
				{
					Operation: &models.Operation{
						Account: mockAccount,
						QueryParams: &models.QueryParams{
							From:  time.Date(2016, time.August, 24, 0, 0, 0, 0, time.UTC),
							Until: time.Date(2016, time.September, 23, 0, 0, 0, 0, time.UTC),
						},
						Err: nil,
					},
					File: []byte("from: 24/08/2016 until: 23/09/2016"),
				},
				{
					Operation: &models.Operation{
						Account: mockAccount,
						QueryParams: &models.QueryParams{
							From:  time.Date(2016, time.September, 24, 0, 0, 0, 0, time.UTC),
							Until: time.Date(2016, time.October, 24, 0, 0, 0, 0, time.UTC),
						},
						Err: nil,
					},
					File: []byte("from: 24/09/2016 until: 24/10/2016"),
				},
				{
					Operation: &models.Operation{
						Account: mockAccount,
						QueryParams: &models.QueryParams{
							From:  time.Date(2016, time.October, 25, 0, 0, 0, 0, time.UTC),
							Until: time.Date(2016, time.November, 24, 0, 0, 0, 0, time.UTC),
						},
						Err: nil,
					},
					File: []byte("from: 25/10/2016 until: 24/11/2016"),
				},
				{
					Operation: &models.Operation{
						Account: mockAccount,
						QueryParams: &models.QueryParams{
							From:  time.Date(2016, time.November, 25, 0, 0, 0, 0, time.UTC),
							Until: time.Date(2016, time.December, 25, 0, 0, 0, 0, time.UTC),
						},
						Err: nil,
					},
					File: []byte("from: 25/11/2016 until: 25/12/2016"),
				},
				{
					Operation: &models.Operation{
						Account: mockAccount,
						QueryParams: &models.QueryParams{
							From:  time.Date(2016, time.December, 26, 0, 0, 0, 0, time.UTC),
							Until: time.Date(2017, time.January, 25, 0, 0, 0, 0, time.UTC),
						},
						Err: nil,
					},
					File: []byte("from: 26/12/2016 until: 25/01/2017"),
				},
				{
					Operation: &models.Operation{
						Account: mockAccount,
						QueryParams: &models.QueryParams{
							From:  time.Date(2017, time.January, 26, 0, 0, 0, 0, time.UTC),
							Until: time.Date(2017, time.February, 25, 0, 0, 0, 0, time.UTC),
						},
						Err: nil,
					},
					File: []byte("from: 26/01/2017 until: 25/02/2017"),
				},
				{
					Operation: &models.Operation{
						Account: mockAccount,
						QueryParams: &models.QueryParams{
							From:  time.Date(2017, time.February, 26, 0, 0, 0, 0, time.UTC),
							Until: time.Date(2017, time.March, 28, 0, 0, 0, 0, time.UTC),
						},
						Err: nil,
					},
					File: []byte("from: 26/02/2017 until: 28/03/2017"),
				},
				{
					Operation: &models.Operation{
						Account: mockAccount,
						QueryParams: &models.QueryParams{
							From:  time.Date(2017, time.March, 29, 0, 0, 0, 0, time.UTC),
							Until: time.Date(2017, time.April, 28, 0, 0, 0, 0, time.UTC),
						},
						Err: nil,
					},
					File: []byte("from: 29/03/2017 until: 28/04/2017"),
				},
				{
					Operation: &models.Operation{
						Account: mockAccount,
						QueryParams: &models.QueryParams{
							From:  time.Date(2017, time.April, 29, 0, 0, 0, 0, time.UTC),
							Until: time.Date(2017, time.May, 23, 0, 0, 0, 0, time.UTC),
						},
						Err: nil,
					},
					File: []byte("from: 29/04/2017 until: 23/05/2017"),
				},
			},
		},
	}

	for k, tst := range tests {
		ch := make(chan *models.QueryResult)
		accounts := []models.Account{
			*tst.account,
		}
		var actualQrs []*models.QueryResult
		doneCh := make(chan bool)
		go func() {
			for qr := range ch {
				actualQrs = append(actualQrs, qr)
			}
			doneCh <- true
		}()
		defer func() {
			if err := recover(); err != nil {
				debug.PrintStack()
				t.Error(err)
			}
		}()
		DownloadPoliciesByDate(accounts, tst.from, tst.until, ch)
		close(ch)
		<-doneCh
		sortedQrs := models.QueryResults(actualQrs)
		sort.Sort(sortedQrs)
		sort.Sort(tst.qr)
		if d := pretty.Compare(tst.qr, sortedQrs); d != "" {
			t.Errorf("%s: results differ:\n%s", k, d)
		}
	}
}

func TestSplitParams(t *testing.T) {
	p := getSplitQueryParams(time.Date(2016, time.January, 1, 0, 0, 0, 0, time.Local),
		time.Date(2017, time.January, 1, 0, 0, 0, 0, time.Local),
		30)
	if l := len(p); l != 12 {
		t.Errorf("wrong length: %d\n", l)
	}
}

func newMock30DayQueryClient(params models.ClientParams) WebsiteClient { return &mock30DayQueryClient{} }
func (c *mock30DayQueryClient) Login() error                           { c.loggedIn = true; return nil }
func (c *mock30DayQueryClient) Logout()                                { c.loggedIn = false }
func (c *mock30DayQueryClient) MaxQueryDays() int                      { return 30 }
func (c *mock30DayQueryClient) RequestRate() time.Duration             { return 0 }
func (c *mock30DayQueryClient) ExportPolicies(from time.Time, until time.Time) ([]byte, error) {
	if !c.loggedIn {
		return nil, errors.New("not logged in")
	}
	if days := until.Sub(from).Hours() / 24; days < 0 || days > 30.0 {
		return nil, fmt.Errorf("bad period: %f", days)
	}
	const dateLayout = "02/01/2006"
	s := fmt.Sprintf("from: %s until: %s", from.Format(dateLayout), until.Format(dateLayout))
	return []byte(s), nil
}

func newMockPageErrorClient(params models.ClientParams) WebsiteClient {
	return &mockPageErrorClient{}
}
func (c *mockPageErrorClient) Login() error               { c.loggedIn = true; return nil }
func (c *mockPageErrorClient) Logout()                    { c.loggedIn = false }
func (c *mockPageErrorClient) MaxQueryDays() int          { return 30 }
func (c *mockPageErrorClient) RequestRate() time.Duration { return 0 }
func (c *mockPageErrorClient) ExportPolicies(from time.Time, until time.Time) ([]byte, error) {
	if !c.loggedIn {
		return nil, errors.New("not logged in")
	}
	if days := until.Sub(from).Hours() / 24; days < 0 || days > 30 {
		return nil, fmt.Errorf("bad period: %.0f", days)
	}
	if from.Month()%2 != 0 {
		return nil, errors.New("odd months are not supported")
	}
	const dateLayout = "02/01/2006"
	s := fmt.Sprintf("from: %s until: %s", from.Format(dateLayout), until.Format(dateLayout))
	return []byte(s), nil
}
