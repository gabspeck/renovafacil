package env

import (
	"database/sql"
	"fmt"

	"os"

	"bitbucket.org/gabspeck/renovafacil/models"
	"bitbucket.org/gabspeck/renovafacil/parsers"
	"github.com/antonlindstrom/pgstore"
	"github.com/gorilla/sessions"
	"upper.io/db.v3/lib/sqlbuilder"
)

type Env struct {
	Config        *models.Config
	DB            sqlbuilder.Database
	QueryResultCh chan *models.QueryResult
	ParseResultCh chan *parsers.ParseResult
	SessionStore  sessions.Store
	DevMode       bool
}

const dbConnectionAttempts = 30

var e *Env

var New = func() (*Env, error) {
	var (
		c     *models.Config
		db    sqlbuilder.Database
		store sessions.Store
		err   error
	)
	if c, err = models.NewConfig(); err != nil {
		return nil, fmt.Errorf("error reading config: %s", err)
	}
	if db, err = models.Connect(os.Getenv("DATABASE_URL"), dbConnectionAttempts); err != nil {
		return nil, fmt.Errorf("error connecting to database: %s", err)
	}
	if err := models.MigrateUp(db.Driver().(*sql.DB)); err != nil {
		return nil, fmt.Errorf("db migration error: %s", err)
	}
	if store, err = createSessionStore(db.Driver().(*sql.DB)); err != nil {
		return nil, fmt.Errorf("error creating session store: %s", err)
	}
	e = &Env{
		Config:        c,
		DB:            db,
		QueryResultCh: make(chan *models.QueryResult),
		ParseResultCh: make(chan *parsers.ParseResult),
		SessionStore:  store,
		DevMode:       os.Getenv("APP_MODE") == "DEV",
	}
	return e, nil
}

func createSessionStore(db *sql.DB) (sessions.Store, error) {

	var authKey = [64]byte{95, 16, 2, 222, 49, 47, 65, 161, 126, 183, 174, 54, 114, 30, 224, 110,
		212, 237, 56, 128, 139, 51, 247, 56, 85, 182, 159, 164, 138, 122, 91, 25,
		43, 245, 119, 54, 222, 141, 239, 178, 139, 221, 205, 125, 166, 25, 158, 11,
		93, 202, 78, 228, 65, 205, 250, 223, 223, 39, 164, 46, 11, 229, 158, 185}

	var cryptoKey = [32]byte{249, 194, 58, 130, 0, 145, 228, 121, 152, 128, 175, 138, 219, 197, 106, 36,
		33, 254, 140, 84, 31, 38, 195, 237, 173, 182, 7, 10, 25, 19, 166, 185}

	store, err := pgstore.NewPGStoreFromPool(db, authKey[:], cryptoKey[:])
	if err != nil {
		return nil, err
	}
	go store.Cleanup(-1)
	return store, nil
}

var Get = func() *Env {
	return e
}
