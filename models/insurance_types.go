package models

import (
	"upper.io/db.v3"
	"upper.io/db.v3/lib/sqlbuilder"
	"strings"
	"errors"
)

const tblInsuranceTypes = "insurance_types"

type InsuranceType struct {
	Entity
	Name string `db:"name"`
}

var (
	ErrNoInsuranceTypeName = errors.New("insurance type name is mandatory")
	ErrNilInsuranceType    = errors.New("insurance type is nil")
)

func InsuranceTypes(db db.Database) db.Collection {
	return db.Collection(tblInsuranceTypes)
}

func AddInsuranceType(db db.Database, t *InsuranceType) (int64, error) {
	if t == nil {
		return 0, ErrNilInsuranceType
	}
	if strings.TrimSpace(t.Name) == "" {
		return 0, ErrNoInsuranceTypeName
	}
	newID, err := InsuranceTypes(db).Insert(t)
	if err != nil {
		return 0, err
	}
	return newID.(int64), err
}

func GetInsuranceTypeIDByName(dbase sqlbuilder.Database, name string) (int64, error) {
	var m map[string]int64
	if err := InsuranceTypes(dbase).Find("name", name).Select("id").One(&m); err != nil && err != db.ErrNoMoreRows {
		return 0, err
	}

	return m["id"], nil
}
