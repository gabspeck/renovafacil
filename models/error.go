package models

import (
	"encoding/json"
	"errors"
	"net/http"
)

type Error interface {
	error
	Status() int
}

type StatusError struct {
	Code int
	Err  error
}

func ValidationError(msg string) StatusError {
	return StatusError{http.StatusBadRequest, errors.New(msg)}
}

func ParseError(err error) Error {
	if err == nil {
		return nil
	}
	switch err.(type) {
	case Error:
		return err.(Error)
	default:
		return StatusError{http.StatusInternalServerError, err}
	}
}

func (se StatusError) Error() string {
	return se.Err.Error()
}

func (se StatusError) Status() int {
	return se.Code
}

func (se StatusError) MarshalJSON() ([]byte, error) {
	return json.Marshal(
		map[string]string{
			"message": se.Error(),
		},
	)
}
