package models

import (
	"time"
)

type Operation struct {
	Account *Account
	*QueryParams
	Err     error
}

type QueryParams struct {
	From  time.Time
	Until time.Time
}

type QueryResult struct {
	*Operation
	File []byte
}

type QueryResults []*QueryResult

func (qr QueryResults) Len() int {
	return len(qr)
}

func (qr QueryResults) Less(i, j int) bool {
	if op := qr[i].Operation; op == nil || op.QueryParams == nil {
		return true
	} else if op = qr[j].Operation; op == nil || op.QueryParams == nil {
		return false
	}

	if from := qr[i].From; from.Before(qr[j].From) {
		return true
	}

	if until := qr[i].Until; until.Before(qr[j].Until) {
		return true
	}

	return false

}

func (qr QueryResults) Swap(i, j int) {
	qr[i], qr[j] = qr[j], qr[i]
}
