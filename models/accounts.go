package models

import (
	"errors"
	"fmt"
	"io/ioutil"
	"strings"

	"gopkg.in/yaml.v2"
	"upper.io/db.v3"
	"upper.io/db.v3/lib/sqlbuilder"
)

const tblAccounts = "accounts"

type Account struct {
	Entity
	CompanyID int64    `db:"company_id" json:"companyID"`
	Name      string   `db:"name" json:"name"`
	Enabled   bool     `db:"enabled" json:"enabled"`
	Username  string   `db:"username" json:"username"`
	Password  string   `db:"password" json:"password"`
	Company   *Company `db:"-" json:"company"`
}

func Accounts(db db.Database) db.Collection {
	return db.Collection(tblAccounts)
}

var (
	ErrNoAccount     = errors.New("account is nil")
	ErrNoAccountName = errors.New("account name is mandatory")
	ErrNoUsername    = errors.New("username is mandatory")
	ErrNoPassword    = errors.New("password is mandatory")
)

func validateAccount(a *Account) error {
	if a == nil {
		return ErrNoAccount
	}

	if strings.TrimSpace(a.Name) == "" {
		return ErrNoAccountName
	}

	if strings.TrimSpace(a.Username) == "" {
		return ErrNoUsername
	}

	if strings.TrimSpace(a.Password) == "" {
		return ErrNoPassword
	}

	return nil

}

func AddAccount(db sqlbuilder.Database, a *Account) (int64, error) {

	var newID interface{}

	if err := validateAccount(a); err != nil {
		return 0, err
	}

	newID, err := Accounts(db).Insert(a)
	if err != nil {
		return 0, err
	}

	return newID.(int64), nil
}

func UpdateAccount(db sqlbuilder.Database, a *Account) error {
	return Accounts(db).UpdateReturning(a);
}

func InitializeAccounts(dbase sqlbuilder.Database, fn string) error {
	f, err := ioutil.ReadFile(fn)
	if err != nil {
		return fmt.Errorf("error reading accounts file at '%s': %s", fn, err)
	}

	accts := map[string]Account{}
	err = yaml.Unmarshal(f, &accts)
	if err != nil {
		return fmt.Errorf("unmarshaling error: %s", err)
	}

	err = dbase.Tx(dbase.Context(), func(tx sqlbuilder.Tx) error {
		for k, v := range accts {
			v.CompanyID, err = GetCompanyIDByName(dbase, v.Company.Name)
			if err != nil {
				return fmt.Errorf("error fetching company: %s", err)
			}
			if v.CompanyID == 0 {
				return fmt.Errorf("no company named '%s' found", v.Company.Name)
			}
			_, err = AddAccount(dbase, &v)
			if err != nil {
				return fmt.Errorf("error adding account '%s': %s", k, err)
			}
		}
		return nil
	})

	return err
}

var GetAccounts = func(dbase sqlbuilder.Database, enabledOnly bool) (accounts []Account, err error) {
	var criteria = db.Cond{}
	if enabledOnly {
		criteria["enabled"] = true
	}
	err = Accounts(dbase).Find(&criteria).OrderBy("name").All(&accounts)
	if err != nil {
		return
	}
	for i := 0; i < len(accounts); i++ {
		account := &accounts[i]
		account.Company, err = GetCompanyByID(dbase, account.CompanyID)
		if err != nil {
			return nil, err
		}
	}
	if accounts == nil {
		accounts = []Account{}
	}
	return
}
