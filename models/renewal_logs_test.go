package models

import (
	"testing"
	"upper.io/db.v3/lib/sqlbuilder"
	"time"
	"github.com/kylelemons/godebug/pretty"
	"github.com/guregu/null"
	"database/sql"
)

func testGetRenewalHistory(db sqlbuilder.Database) func(*testing.T) {
	return func(t *testing.T) {
		tests := map[string]struct {
			rpp   uint
			page  uint
			total int
			first RenewalHistoryEntry
			last  RenewalHistoryEntry
			err   error
		}{
			"page 1": {
				rpp:   5,
				page:  1,
				total: 10,
				first: RenewalHistoryEntry{
					ID:            1001,
					Customer:      "Neve P. Cote",
					OldAccount:    "Royal Insurance Co.",
					Number:        "80728134799000",
					InsuranceType: "Health",
					Description:   "rhoncus id, mollis nec, cursus a,",
					NewAccount:    nil,
					RenewedOn:     time.Date(2017, time.November, 16, 0, 0, 0, 0, time.FixedZone("", 0)),
					Total:         10,
				},
				last: RenewalHistoryEntry{
					ID:            1009,
					Customer:      "Brenda A. Stewart",
					OldAccount:    "Royal Insurance Co.",
					Number:        "62620262899000",
					InsuranceType: "Hush money",
					Description:   "Integer eu lacus. Quisque",
					NewAccount:    &null.String{sql.NullString{"Insurances 'R' Us", true}},
					RenewedOn:     time.Date(2017, time.September, 12, 0, 0, 0, 0, time.FixedZone("", 0)),
					Total:         10,
				},
				err: nil,
			},
			"page 2": {
				rpp:   5,
				page:  2,
				total: 10,
				first: RenewalHistoryEntry{
					ID:            1007,
					Customer:      "Raphael Y. Weiss",
					OldAccount:    "Insurances 'R' Us",
					Number:        "00973817099000",
					InsuranceType: "Body parts",
					Description:   "mi lorem, vehicula et, rutrum",
					NewAccount:    &null.String{sql.NullString{"Royal Insurance Co.", true}},
					RenewedOn:     time.Date(2017, time.September, 3, 0, 0, 0, 0, time.FixedZone("", 0)),
					Total:         10,
				},
				last: RenewalHistoryEntry{
					ID:            1004,
					Customer:      "Remedios N. Mayer",
					OldAccount:    "Insurances 'R' Us",
					Number:        "73931972199000",
					InsuranceType: "Health",
					Description:   "fermentum metus. Aenean sed pede",
					NewAccount:    &null.String{sql.NullString{"Felcher & Sons", true}},
					RenewedOn:     time.Date(2017, time.May, 27, 0, 0, 0, 0, time.FixedZone("", 0)),
					Total:         10,
				},
				err: nil,
			},
		}
		for k, test := range tests {
			total, res, err := GetRenewalHistory(db, test.rpp, test.page)
			if err != test.err {
				t.Errorf("GetRenewalHistory [%s]: wanted error %s, got %s", k, test.err, err)
			}

			if total != test.total {
				t.Errorf("GetRenewalHistory [%s]: wanted total %d, got %d", k, test.total, total)
			}

			if d := pretty.Compare(test.first, res[0]); d != "" {
				t.Errorf("GetRenewalHistory [%s]: first result differs:\n%s", k, d)
			}

			if d := pretty.Compare(test.last, res[len(res)-1]); d != "" {
				t.Errorf("GetRenewalHistory [%s]: last result differs:\n%s", k, d)
			}

		}
	}
}

func testDeleteRenewalLogByID(db sqlbuilder.Database) func(*testing.T) {
	return func(t *testing.T) {
		tests := map[string]struct {
			id  int64
			err error
		}{
			"valid ID": {
				id:  1009,
				err: nil,
			},
		}
		for k, test := range tests {
			err := DeleteRenewalLogByID(db, test.id)
			if err != test.err {
				t.Errorf("DeleteRenewalLogByID [%s]: wanted error %s, got %s", k, test.err, err)
			}
			if test.err != nil {
				exists, err := db.Collection("renewal_logs").Find("id", test.id).Exists()
				if err != nil {
					t.Errorf("DeleteRenewalLogByID [%s]: error checking deletion: %s", k, err)
				}
				if exists {
					t.Errorf("DeleteRenewalLogByID [%s]: log renewal with ID %d not deleted", k, test.id)
				}
			}
		}
	}
}
