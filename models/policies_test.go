package models

import (
	"fmt"
	"testing"
	"time"

	"github.com/kylelemons/godebug/pretty"
	"upper.io/db.v3/lib/sqlbuilder"
	"database/sql"
	"runtime/debug"
)

func testGetPendingRenewalsByPeriodEnd(db sqlbuilder.Database) func(*testing.T) {
	return func(t *testing.T) {
		type test struct {
			end   time.Time
			rpp   uint
			page  uint
			total int
			first *ExpiringPolicy
			last  *ExpiringPolicy
			err   error
		}
		tests := map[string]test{
			"page one": {
				end:   time.Date(2017, time.November, 1, 0, 0, 0, 0, time.UTC),
				rpp:   10,
				page:  1,
				total: 31,
				err:   nil,
				first: &ExpiringPolicy{
					ID:            1008,
					AccountID:     1000,
					Account:       "Insurances 'R' Us",
					Customer:      "Nevada I. Church",
					Description:   "primis in faucibus orci",
					Email:         "vestibulum.Mauris.magna@Proin.edu",
					InsuranceType: "Body parts",
					Number:        "95627090699000",
					PeriodEnd:     time.Date(2017, time.November, 3, 0, 0, 0, 0, time.FixedZone("", 0)),
					PhoneNumber:   "(899) 989-8344",
					Total:         31,
				},
				last: &ExpiringPolicy{
					ID:            1004,
					AccountID:     1000,
					Account:       "Insurances 'R' Us",
					Customer:      "Veda K. Compton",
					Description:   "tristique pharetra. Quisque ac libero nec",
					Email:         "enim@eueleifendnec.com",
					InsuranceType: "Body parts",
					Number:        "51289577599000",
					PeriodEnd:     time.Date(2017, time.November, 21, 0, 0, 0, 0, time.FixedZone("", 0)),
					PhoneNumber:   "(360) 912-7024",
					Total:         31,
				},
			},
			"page two": {
				end:   time.Date(2017, time.November, 1, 0, 0, 0, 0, time.UTC),
				rpp:   10,
				page:  2,
				total: 31,
				err:   nil,
				first: &ExpiringPolicy{
					ID:            1038,
					AccountID:     1000,
					Account:       "Insurances 'R' Us",
					Customer:      "Harding L. Leblanc",
					Description:   "dictum. Phasellus in felis. Nulla",
					Email:         "in.cursus@nonenim.com",
					InsuranceType: "Body parts",
					Number:        "88439124999000",
					PeriodEnd:     time.Date(2017, time.November, 22, 0, 0, 0, 0, time.FixedZone("", 0)),
					PhoneNumber:   "(784) 390-4624",
					Total:         31,
				},
				last: &ExpiringPolicy{
					ID:            1049,
					AccountID:     1001,
					Account:       "Felcher & Sons",
					Customer:      "Rebecca V. Durham",
					Description:   "consectetuer mauris id sapien. Cras",
					Email:         "lobortis.nisi@arcuvelquam.com",
					InsuranceType: "Body parts",
					Number:        "61205002999000",
					PeriodEnd:     time.Date(2017, time.December, 2, 0, 0, 0, 0, time.FixedZone("", 0)),
					PhoneNumber:   "(305) 109-7117",
					Total:         31,
				},
			},
		}

		for k, tst := range tests {
			actualTotal, pols, actualErr := GetPendingRenewalsByPeriodEnd(db, tst.rpp, tst.page, tst.end)
			if fmt.Sprint(actualErr) != fmt.Sprint(tst.err) {
				t.Errorf("%s: wanted error %s, got %s", k, tst.err, actualErr)
			}
			if actualTotal != tst.total {
				t.Errorf("%s: wanted total %d, got %d", k, tst.total, actualTotal)
			}
			if actualLen := uint(len(pols)); actualLen != tst.rpp {
				t.Errorf("%s: wanted %d results, got %d", k, tst.rpp, actualLen)
			}

			if len(pols) > 0 {
				if c := pretty.Compare(tst.first, pols[0]); c != "" {
					t.Errorf("%s: unexpected first result:\n%s", k, c)
				}

			}

		}
	}
}

func testAddPolicy(db sqlbuilder.Database) func(*testing.T) {

	return func(t *testing.T) {

		type test struct {
			p   *Policy
			id  int64
			err error
		}

		tests := map[string]test{
			"valid policy": {
				p: &Policy{
					AccountID:     999,
					InsuranceType: &InsuranceType{Name: "Body parts"},
					Customer: &Customer{
						Name:        "Aleister Crowley",
						TaxID:       "0001112233345",
						Email:       sql.NullString{"ac@example.com", true},
						PhoneNumber: "+1 800 6666666",
					},
					Description: "Bubble butt",
					Number:      "123457984444",
					RootNumber:  "12345798",
					PeriodStart: time.Date(2017, 5, 1, 0, 0, 0, 0, time.UTC),
					PeriodEnd:   time.Date(2018, 5, 1, 0, 0, 0, 0, time.UTC),
				},
				id:  1,
				err: nil,
			},
			"no account ID": {
				p: &Policy{
					AccountID:     0,
					InsuranceType: &InsuranceType{Name: "Body parts"},
					Customer: &Customer{
						Name:        "Aleister Crowley",
						TaxID:       "0001112233345",
						Email:       sql.NullString{"ac@example.com", true},
						PhoneNumber: "+1 800 6666666",
					},
					Description: "Bubble butt",
					Number:      "123457984444",
					RootNumber:  "12345798",
					PeriodStart: time.Date(2017, 5, 1, 0, 0, 0, 0, time.UTC),
					PeriodEnd:   time.Date(2018, 5, 1, 0, 0, 0, 0, time.UTC),
				},
				id:  0,
				err: ErrNoAccount,
			},
			"no insurance type": {
				p: &Policy{
					AccountID:     999,
					InsuranceType: &InsuranceType{Name: ""},
					Customer: &Customer{
						Name:        "Aleister Crowley",
						TaxID:       "0001112233345",
						Email:       sql.NullString{"ac@example.com", true},
						PhoneNumber: "+1 800 6666666",
					},
					Description: "Bubble butt",
					Number:      "123457984444",
					RootNumber:  "12345798",
					PeriodStart: time.Date(2017, 5, 1, 0, 0, 0, 0, time.UTC),
					PeriodEnd:   time.Date(2018, 5, 1, 0, 0, 0, 0, time.UTC),
				},
				id:  0,
				err: ErrNoType,
			},
			"nil customer": {
				p: &Policy{
					AccountID:     999,
					InsuranceType: &InsuranceType{Name: "Body parts"},
					Customer:      nil,
					Description:   "Bubble butt",
					Number:        "123457984444",
					RootNumber:    "12345798",
					PeriodStart:   time.Date(2017, 5, 1, 0, 0, 0, 0, time.UTC),
					PeriodEnd:     time.Date(2018, 5, 1, 0, 0, 0, 0, time.UTC),
				},
				id:  0,
				err: ErrNilCustomer,
			},
			"no description": {
				p: &Policy{
					AccountID:     999,
					InsuranceType: &InsuranceType{Name: "Body parts"},
					Customer: &Customer{
						Name:        "Aleister Crowley",
						TaxID:       "0001112233345",
						Email:       sql.NullString{"ac@example.com", true},
						PhoneNumber: "+1 800 6666666",
					},
					Description: "",
					Number:      "123457984444",
					RootNumber:  "12345798",
					PeriodStart: time.Date(2017, 5, 1, 0, 0, 0, 0, time.UTC),
					PeriodEnd:   time.Date(2018, 5, 1, 0, 0, 0, 0, time.UTC),
				},
				id:  0,
				err: ErrNoDescription,
			},
			"no number": {
				p: &Policy{
					AccountID:     999,
					InsuranceType: &InsuranceType{Name: "Body parts"},
					Customer: &Customer{
						Name:        "Aleister Crowley",
						TaxID:       "0001112233345",
						Email:       sql.NullString{"ac@example.com", true},
						PhoneNumber: "+1 800 6666666",
					},
					Description: "Bubble butt",
					Number:      "",
					RootNumber:  "12345798",
					PeriodStart: time.Date(2017, 5, 1, 0, 0, 0, 0, time.UTC),
					PeriodEnd:   time.Date(2018, 5, 1, 0, 0, 0, 0, time.UTC),
				},
				id:  0,
				err: ErrNoNumber,
			},
			"no start date": {
				p: &Policy{
					AccountID:     999,
					InsuranceType: &InsuranceType{Name: "Body parts"},
					Customer: &Customer{
						Name:        "Aleister Crowley",
						TaxID:       "0001112233345",
						Email:       sql.NullString{"ac@example.com", true},
						PhoneNumber: "+1 800 6666666",
					},
					Description: "Bubble butt",
					Number:      "123457984444",
					RootNumber:  "12345798",
					PeriodStart: time.Time{},
					PeriodEnd:   time.Date(2018, 5, 1, 0, 0, 0, 0, time.UTC),
				},
				id:  0,
				err: ErrBadPeriod,
			},
			"no end date": {
				p: &Policy{
					AccountID:     999,
					InsuranceType: &InsuranceType{Name: "Body parts"},
					Customer: &Customer{
						Name:        "Aleister Crowley",
						TaxID:       "0001112233345",
						Email:       sql.NullString{"ac@example.com", true},
						PhoneNumber: "+1 800 6666666",
					},
					Description: "Bubble butt", Number: "123457984444",
					RootNumber:  "12345798",
					PeriodStart: time.Date(2017, 5, 1, 0, 0, 0, 0, time.UTC),
					PeriodEnd:   time.Time{},
				},
				id:  0,
				err: ErrBadPeriod,
			},
			"no start and end dates": {
				p: &Policy{
					AccountID:     999,
					InsuranceType: &InsuranceType{Name: "Body parts"},
					Customer: &Customer{
						Name:        "Aleister Crowley",
						TaxID:       "0001112233345",
						Email:       sql.NullString{"ac@example.com", true},
						PhoneNumber: "+1 800 6666666",
					},
					Description: "Bubble butt",
					Number:      "123457984444",
					RootNumber:  "12345798",
					PeriodStart: time.Time{},
					PeriodEnd:   time.Time{},
				},
				id:  0,
				err: ErrBadPeriod,
			},
			"start date after end date": {
				p: &Policy{
					AccountID:     999,
					InsuranceType: &InsuranceType{Name: "Body parts"},
					Customer: &Customer{
						Name:        "Aleister Crowley",
						TaxID:       "0001112233345",
						Email:       sql.NullString{"ac@example.com", true},
						PhoneNumber: "+1 800 6666666",
					},
					Description: "Bubble butt",
					Number:      "123457984444",
					RootNumber:  "12345798",
					PeriodStart: time.Date(2018, 5, 1, 0, 0, 0, 0, time.UTC),
					PeriodEnd:   time.Date(2017, 5, 1, 0, 0, 0, 0, time.UTC),
				},
				id:  0,
				err: ErrPeriodStartAfterEnd,
			},
			"AddCustomer error": {
				p: &Policy{
					AccountID:     999,
					InsuranceType: &InsuranceType{Name: "Body parts"},
					Customer: &Customer{
						Name:        "",
						TaxID:       "0X1BADCAFE",
						Email:       sql.NullString{"ac@example.com", true},
						PhoneNumber: "+1 800 6666666",
					},
					Description: "Bubble butt",
					Number:      "123457984444",
					RootNumber:  "12345798",
					PeriodStart: time.Date(2017, 5, 1, 0, 0, 0, 0, time.UTC),
					PeriodEnd:   time.Date(2018, 5, 1, 0, 0, 0, 0, time.UTC),
				},
				id:  0,
				err: fmt.Errorf("error adding customer: %s", ErrNoName),
			},
			"nil insurance type": {
				p: &Policy{
					AccountID:     999,
					InsuranceType: nil,
					Customer: &Customer{
						Name:        "Aleister Crowley",
						TaxID:       "0001112233345",
						Email:       sql.NullString{"ac@example.com", true},
						PhoneNumber: "+1 800 6666666",
					},
					Description: "Bubble butt",
					Number:      "123457984444",
					RootNumber:  "12345798",
					PeriodStart: time.Date(2017, 5, 1, 0, 0, 0, 0, time.UTC),
					PeriodEnd:   time.Date(2018, 5, 1, 0, 0, 0, 0, time.UTC),
				},
				id:  0,
				err: ErrNoType,
			},
			"no root number": {
				p: &Policy{
					AccountID:     999,
					InsuranceType: &InsuranceType{Name: "Body parts"},
					Customer: &Customer{
						Name:        "Aleister Crowley",
						TaxID:       "0001112233345",
						Email:       sql.NullString{"ac@example.com", true},
						PhoneNumber: "+1 800 6666666",
					},
					Description: "Bubble butt",
					Number:      "123457984444",
					RootNumber:  "",
					PeriodStart: time.Date(2017, 5, 1, 0, 0, 0, 0, time.UTC),
					PeriodEnd:   time.Date(2018, 5, 1, 0, 0, 0, 0, time.UTC),
				},
				id:  0,
				err: ErrNoRootNumber,
			},
		}
		defer func() {
			if err := recover(); err != nil {
				t.Errorf("panic: %s", err)
				debug.PrintStack()
			}

		}()
		for k, tst := range tests {
			actualID, actualErr := AddPolicy(db, tst.p)
			if fmt.Sprint(actualErr) != fmt.Sprint(tst.err) {
				t.Errorf("add policy [%s]: wanted error %s, got %s", k, tst.err, actualErr)
			}
			if actualID != tst.id {
				t.Errorf("add policy [%s]: wanted ID %d, got %d", k, tst.id, actualID)
			}

		}
	}
}
