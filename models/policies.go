package models

import (
	"time"
	"upper.io/db.v3"
	"upper.io/db.v3/lib/sqlbuilder"
	"errors"
	"fmt"
	"strings"
)

const tblPolicies = "policies"

type Policy struct {
	Entity
	CustomerID          int64          `db:"customer_id"`
	AccountID           int64          `db:"account_id"`
	InsuranceTypeID     int64          `db:"insurance_type_id"`
	Number              string         `db:"policy_number"`
	RootNumber          string         `db:"root_number"`
	EndorsementSequence int            `db:"endorsement_sequence"`
	Description         string         `db:"description"`
	PeriodStart         time.Time      `db:"period_start"`
	PeriodEnd           time.Time      `db:"period_end"`
	Customer            *Customer      `db:"-"`
	Account             *Account       `db:"-"`
	InsuranceType       *InsuranceType `db:"-"`
}

type PolicySlice []Policy

func (p PolicySlice) Len() int           { return len(p) }
func (p PolicySlice) Less(i, j int) bool { return p[i].Number < p[j].Number }
func (p PolicySlice) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

func Policies(db db.Database) db.Collection {
	return db.Collection(tblPolicies)
}

type ExpiringPolicy struct {
	ID            int64     `db:"id" json:"id"`
	Customer      string    `db:"customer" json:"customer"`
	AccountID     int64     `db:"account_id" json:"accountID"`
	Account       string    `db:"account" json:"account"`
	Number        string    `db:"number" json:"number"`
	InsuranceType string    `db:"insurance_type" json:"insuranceType"`
	Description   string    `db:"description" json:"description"`
	PeriodEnd     time.Time `db:"period_end" json:"periodEnd"`
	PhoneNumber   string    `db:"phone_number" json:"phoneNumber"`
	Email         string    `db:"email" json:"email"`
	Total         int       `db:"total" json:"-"`
}

var (
	ErrNilPolicy           = errors.New("policy is nil")
	ErrNilCustomer         = errors.New("customer is nil")
	ErrNoNumber            = errors.New("number is blank")
	ErrNoRootNumber        = errors.New("root number is blank")
	ErrNoType              = errors.New("type is blank")
	ErrNoDescription       = errors.New("description is blank")
	ErrBadPeriod           = errors.New("policy period blank or incomplete")
	ErrPeriodStartAfterEnd = errors.New("period start after end")
)

func AddPolicy(db sqlbuilder.Database, p *Policy) (int64, error) {
	if err := validatePolicy(db, p); err != nil {
		return 0, err
	}

	err := db.Tx(db.Context(), func(tx sqlbuilder.Tx) error {
		var (
			err   error
			newID interface{}
		)
		if p.CustomerID, err = GetCustomerIDByTaxID(db, p.Customer.TaxID); err != nil {
			return fmt.Errorf("error getting customer ID: %s", err)
		}
		if p.CustomerID == 0 {
			if p.CustomerID, err = AddCustomer(tx, p.Customer); err != nil {
				return fmt.Errorf("error adding customer: %s", err)
			}
		}
		if p.InsuranceTypeID, err = GetInsuranceTypeIDByName(db, p.InsuranceType.Name); err != nil {
			return fmt.Errorf("error fetching insurance type: %s", err)
		}
		if p.InsuranceTypeID == 0 {
			if p.InsuranceTypeID, err = AddInsuranceType(db, p.InsuranceType); err != nil {
				return fmt.Errorf("error inserting insurance type: %s", err)
			}
		}
		newID, err = Policies(tx).Insert(p)
		if err != nil {
			return err
		}
		p.ID = newID.(int64)
		return nil
	})
	if err != nil {
		return 0, err
	}
	return p.ID, nil
}

func validatePolicy(db db.Database, p *Policy) error {
	if db == nil {
		return errors.New("database is nil")
	}

	if p == nil {
		return ErrNilPolicy
	}

	if p.Customer == nil {
		return ErrNilCustomer
	}

	if p.AccountID == 0 {
		return ErrNoAccount
	}

	if number := strings.TrimSpace(p.Number); number == "" {
		return ErrNoNumber
	}

	if rootNumber := strings.TrimSpace(p.RootNumber); rootNumber == "" {
		return ErrNoRootNumber
	}

	if insuranceType := p.InsuranceType; insuranceType == nil || strings.TrimSpace(insuranceType.Name) == "" {
		return ErrNoType
	}

	if description := strings.TrimSpace(p.Description); description == "" {
		return ErrNoDescription
	}

	if p.PeriodStart.IsZero() || p.PeriodEnd.IsZero() {
		return ErrBadPeriod
	}

	if p.PeriodStart.After(p.PeriodEnd) {
		return ErrPeriodStartAfterEnd
	}

	return nil
}

func GetPendingRenewalsByPeriodEnd(db sqlbuilder.Database, resultsPerPage, page uint, periodEnd time.Time) (total int, policies []ExpiringPolicy, err error) {
	actualPage := page - 1
	offset := actualPage * resultsPerPage

	r, err := db.Query(`select p.id as id, c.id as customer_id, c.name as customer, ac.id as account_id, ac.name as account, p.policy_number as number,
		t.name as insurance_type, p.description as description, p.period_end,
		c.phone_number, coalesce(c.email, '') as email, count(p.id) over() as total
		from active_policies p
		left join renewal_logs rl on rl.policy_id = p.id
		join customers c on c.id = p.customer_id
		join accounts ac on ac.id = p.account_id
		join insurance_types t on t.id = p.insurance_type_id
		where
		p.period_end >= ? and
		rl.id is null
		order by p.period_end, customer, customer_id limit ? offset ?`, periodEnd, resultsPerPage, offset)

	if err != nil {
		return
	}
	iter := sqlbuilder.NewIterator(r)
	defer iter.Close()
	if err = iter.All(&policies); err != nil {
		return
	}

	if len(policies) > 0 {
		total = policies[0].Total
	}

	return

}
