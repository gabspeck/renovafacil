package models

import (
	"testing"

	"github.com/kylelemons/godebug/pretty"
	"upper.io/db.v3/lib/sqlbuilder"
)

func testAddAccount(db sqlbuilder.Database) func(*testing.T) {
	return func(t *testing.T) {
		tsts := map[string]struct {
			a   *Account
			id  int64
			err error
		}{
			"valid account": {
				a: &Account{
					CompanyID: 999,
					Name:      "Conta 1",
					Username:  "admin",
					Password:  "12345",
				},
				id:  3,
				err: nil,
			},
			"no account name": {
				a: &Account{
					CompanyID: 999,
					Name:      "",
					Username:  "admin",
					Password:  "12345",
				},
				id:  0,
				err: ErrNoAccountName,
			},
			"no username": {
				a: &Account{
					CompanyID: 999,
					Name:      "Conta 1",
					Username:  "",
					Password:  "12345",
				},
				id:  0,
				err: ErrNoUsername,
			},
			"no password": {
				a: &Account{
					CompanyID: 999,
					Name:      "Conta 1",
					Username:  "admin",
					Password:  "",
				},
				id:  0,
				err: ErrNoPassword,
			},
		}
		for k, v := range tsts {
			actualID, actualErr := AddAccount(db, v.a)
			if c := pretty.Compare(v.id, actualID); c != "" {
				t.Errorf("%s: unexpected ID:\n%s", k, c)
			}
			if c := pretty.Compare(v.err, actualErr); c != "" {
				t.Errorf("%s: unexpected error:\n%s", k, c)
			}
		}

	}

}

func testGetAccounts(dbase sqlbuilder.Database) func(*testing.T) {
	return func(t *testing.T) {
		var tests = map[string]struct {
			enabledOnly bool
			ret         []Account
		}{
			"enabled only": {
				enabledOnly: true,
				ret: []Account{
					{
						Entity:    Entity{ID: 1001},
						CompanyID: 1001,
						Name:      "Felcher & Sons",
						Enabled:   true,
						Username:  "alibaba",
						Password:  "40thieves",
						Company: &Company{
							Entity: Entity{ID: 1001},
							Name:   "Felcher & Sons",
						},
					},
					{
						Entity:    Entity{ID: 1000},
						CompanyID: 1000,
						Name:      "Insurances 'R' Us",
						Enabled:   true,
						Username:  "jasmine",
						Password:  "rajah",
						Company: &Company{
							Entity: Entity{ID: 1000},
							Name:   "Insurances 'R' Us",
						},
					},
					{
						Entity:    Entity{ID: 999},
						CompanyID: 999,
						Name:      "Royal Insurance Co.",
						Enabled:   true,
						Username:  "aladdin",
						Password:  "opensesame",
						Company: &Company{
							Entity: Entity{ID: 999},
							Name:   "Royal Insurance Co.",
						},
					},
				},
			},
			"all": {
				enabledOnly: false,
				ret: []Account{
					{
						Entity:    Entity{ID: 1001},
						CompanyID: 1001,
						Name:      "Felcher & Sons",
						Enabled:   true,
						Username:  "alibaba",
						Password:  "40thieves",
						Company: &Company{
							Entity: Entity{ID: 1001},
							Name:   "Felcher & Sons",
						},
					},
					{
						Entity:    Entity{ID: 1002},
						CompanyID: 1001,
						Name:      "Felcher & Sons - Disabled",
						Enabled:   false,
						Username:  "jaffar",
						Password:  "somethingelse",
						Company: &Company{
							Entity: Entity{ID: 1001},
							Name:   "Felcher & Sons",
						},
					},
					{
						Entity:    Entity{ID: 1000},
						CompanyID: 1000,
						Name:      "Insurances 'R' Us",
						Enabled:   true,
						Username:  "jasmine",
						Password:  "rajah",
						Company: &Company{
							Entity: Entity{ID: 1000},
							Name:   "Insurances 'R' Us",
						},
					},
					{
						Entity:    Entity{ID: 999},
						CompanyID: 999,
						Name:      "Royal Insurance Co.",
						Enabled:   true,
						Username:  "aladdin",
						Password:  "opensesame",
						Company: &Company{
							Entity: Entity{ID: 999},
							Name:   "Royal Insurance Co.",
						},
					},
				},
			},
		}
		for k, test := range tests {
			actual, err := GetAccounts(dbase, test.enabledOnly)
			if err != nil {
				t.Errorf("GetAccounts [%s]: error: %s", k, err)
			}
			if c := pretty.Compare(test.ret, actual); c != "" {
				t.Errorf("GetAccounts [%s]: results differ from expected:\n%s", k, c)
			}
		}
	}
}

func testInitializeAccounts(db sqlbuilder.Database) func(*testing.T) {
	return func(t *testing.T) {
		tsts := map[string]struct {
			fn    string
			accts []Account
			isErr bool
		}{
			"correct file": {
				fn: "testdata/accounts.yml",
				accts: []Account{
					{
						Name:      "imported account 1",
						CompanyID: 999,
						Username:  "aladdin",
						Password:  "opensesame",
					},
					{
						Name:      "imported account 2",
						CompanyID: 1001,
						Username:  "jasmine",
						Password:  "rajah",
					},
				},
				isErr: false,
			},
		}
		for k, tst := range tsts {
			err := InitializeAccounts(db, tst.fn)
			if hasErr := err != nil; hasErr != tst.isErr {
				t.Errorf("InitializeAccounts [%s]: error status mismatch. should be %v, is %v (err: %s)", k, tst.isErr, hasErr, err)
			}
			dbAccounts := db.Collection("accounts")
			for _, eA := range tst.accts {
				f := dbAccounts.Find("name", eA.Name)
				if cnt, err := f.Count(); err != nil {
					t.Errorf("InitializeAccounts [%s]: error getting count: %s", k, err)
				} else if cnt != 1 {
					t.Errorf("InitializeAccounts [%s]: bad count for account %s: %d", k, eA.Name, cnt)
				}
				actualAccount := &Account{}
				if err := f.One(actualAccount); err != nil {
					t.Errorf("InitializeAccounts [%s]: error getting account: %s", k, err)
				}
				if d := pretty.Compare(eA.Name, actualAccount.Name); d != "" {
					t.Errorf("InitializeAccounts [%s]: account names differ:\n%s", k, d)
				}
				if d := pretty.Compare(eA.CompanyID, actualAccount.CompanyID); d != "" {
					t.Errorf("InitializeAccounts [%s]: company IDs differ:\n%s", k, d)
				}
				if d := pretty.Compare(eA.Username, actualAccount.Username); d != "" {
					t.Errorf("InitializeAccounts [%s]: usernames differ:\n%s", k, d)
				}
				if d := pretty.Compare(eA.Password, actualAccount.Password); d != "" {
					t.Errorf("InitializeAccounts [%s]: passwords differ:\n%s", k, d)
				}
			}
		}
	}
}
