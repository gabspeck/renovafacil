package models

import (
	"testing"

	"upper.io/db.v3/lib/sqlbuilder"
)

func testGetInsuranceTypeIDByName(db sqlbuilder.Database) func(*testing.T) {
	return func(t *testing.T) {
		tsts := map[string]struct {
			name string
			id   int64
			err  error
		}{
			"existing type": {
				name: "Health",
				id:   1001,
				err:  nil,
			},
			"type not found": {
				name: "Beelzebub",
				id:   0,
				err:  nil,
			},
		}

		for k, tst := range tsts {
			actualID, actualErr := GetInsuranceTypeIDByName(db, tst.name)
			if actualErr != tst.err {
				t.Errorf("GetInsuranceTypeIDByName [%s]: wanted error %s, got %s", k, tst.err, actualErr)
			}
			if actualID != tst.id {
				t.Errorf("GetInsuranceTypeIDByName [%s]: id should be %d, got %d", k, tst.id, actualID)
			}
		}
	}
}

func testAddInsuranceType(db sqlbuilder.Database) func(t *testing.T) {
	return func(t *testing.T) {
		tsts := map[string]struct {
			it  *InsuranceType
			err error
		}{
			"blank name": {
				it: &InsuranceType{
					Name: "",
				},
				err: ErrNoInsuranceTypeName,
			},
			"nil type": {
				it:  nil,
				err: ErrNilInsuranceType,
			},
			"valid type": {
				it: &InsuranceType{
					Name: "Ramo de teste",
				},
				err: nil,
			},
		}

		for k, tst := range tsts {
			actualID, actualErr := AddInsuranceType(db, tst.it)
			if actualErr != tst.err {
				t.Errorf("AddInsuranceType [%s]: wanted error %s, got %s", k, tst.err, actualErr)
			}
			if tst.err == nil && actualID == 0 {
				t.Errorf("AddInsuranceType [%s]: ID is zero but it shouldn't", k)
			}
			if tst.err != nil && actualID != 0 {
				t.Errorf("AddInsuranceType [%s]: ID should be zero but is %d", k, actualID)
			}
		}
	}
}
