package models

import (
	"database/sql"
	"fmt"
	"github.com/mattes/migrate"
	"github.com/mattes/migrate/database/postgres"
	_ "github.com/mattes/migrate/source/file"
	"upper.io/db.v3/lib/sqlbuilder"
	"upper.io/db.v3/postgresql"
	"upper.io/db.v3"
	"time"
	"log"
)

const connectionAttemptInterval = time.Second

type Entity struct {
	ID int64 `db:"id,omitempty" json:"id"`
}

func MigrateUp(db *sql.DB) error {
	var (
		m   *migrate.Migrate
		err error
	)
	if m, err = newMigrate(db, "models/migrations"); err != nil {
		return err
	}
	if err := m.Up(); err != nil && err != migrate.ErrNoChange {
		return err
	}
	return nil
}

func ConnectDefault(c *Config) (sqlbuilder.Database, error) {
	dbCfg := c.DB
	settings := postgresql.ConnectionURL{
		Host:     fmt.Sprintf("%s:%s", dbCfg.Host, dbCfg.Port),
		Database: dbCfg.Name,
		User:     dbCfg.User,
		Password: dbCfg.Password,
	}
	session, err := postgresql.Open(settings)
	if err != nil {
		return nil, err
	}
	return session, nil

}

func Connect(url string, tries int) (dbase sqlbuilder.Database, err error){
	parsedURL, err := postgresql.ParseURL(url)
	if err != nil {
		return nil, err
	}
	for i := 0; i < tries && dbase == nil; i++ {
		if i > 0 {
			log.Printf("error connecting to database at %s: %s; retrying in %d second(s)\n", parsedURL.Host, err, connectionAttemptInterval / time.Second)
			time.Sleep(time.Second)
		}
		dbase, err = postgresql.Open(parsedURL)
	}
	return
}

func GetEntityByID(coll db.Collection, id int64, destPtr interface{}) error {
	if err := coll.Find("id", id).One(destPtr); err != nil && err != db.ErrNoMoreRows {
		return err
	}
	return nil
}

func newMigrate(db *sql.DB, migrationsPath string) (*migrate.Migrate, error) {
	drv, err := postgres.WithInstance(db, &postgres.Config{})
	if err != nil {
		return nil, err
	}
	m, err := migrate.NewWithDatabaseInstance("file://"+migrationsPath, "postgres", drv)
	if err != nil {
		return nil, err
	}
	return m, nil
}
