package models

import (
	"database/sql"
	"upper.io/db.v3"
	"errors"
	"strings"
	"upper.io/db.v3/lib/sqlbuilder"
)

const tblCustomers = "customers"

type Customer struct {
	Entity
	Name        string         `db:"name"`
	TaxID       string         `db:"tax_id"`
	Email       sql.NullString `db:"email"`
	PhoneNumber string         `db:"phone_number"`
}

var (
	ErrNoName        = errors.New("customer name is blank")
	ErrNoTaxID       = errors.New("customer tax ID is blank")
	ErrNoPhoneNumber = errors.New("customer phone number is blank")
)

func Customers(db db.Database) db.Collection {
	return db.Collection(tblCustomers)
}

func AddCustomer(db db.Database, c *Customer) (int64, error) {
	var custName, taxID, custPhone string

	if custName = strings.TrimSpace(c.Name); custName == "" {
		return 0, ErrNoName
	}

	if taxID = strings.TrimSpace(c.TaxID); taxID == "" {
		return 0, ErrNoTaxID
	}

	if custPhone = strings.TrimSpace(c.PhoneNumber); custPhone == "" {
		return 0, ErrNoPhoneNumber
	}

	email := sql.NullString{}
	if c.Email.Valid {
		email.Scan(strings.TrimSpace(c.Email.String))
	}

	dbCustomer := &Customer{
		Name:        custName,
		Email:       email,
		TaxID:       taxID,
		PhoneNumber: custPhone,
	}

	newID, err := Customers(db).Insert(dbCustomer)
	if err != nil {
		return 0, err
	}
	return newID.(int64), nil
}

func GetCustomerIDByTaxID(dbase sqlbuilder.Database, taxID string) (int64, error) {
	var m map[string]int64
	if err := Customers(dbase).Find("tax_id", taxID).Select("id").One(&m); err != nil && err != db.ErrNoMoreRows {
		return 0, err
	}
	return m["id"], nil
}
