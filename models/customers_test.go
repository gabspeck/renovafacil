package models

import (
	"database/sql"
	"testing"

	"upper.io/db.v3"
)

func testAddCustomer(db db.Database) func(*testing.T) {
	return func(t *testing.T) {
		type test struct {
			c       *Customer
			validID bool
			err     error
		}

		tests := map[string]test{
			"valid customer": {
				c: &Customer{
					Name:        "Quincy Jones",
					TaxID:       "111222333444",
					Email:       sql.NullString{"quincy@jones.com", true},
					PhoneNumber: "212 5555 5555",
				},
				validID: true,
				err:     nil,
			},
			"no name": {
				c: &Customer{
					Name:        "",
					TaxID:       "111222333444",
					Email:       sql.NullString{"quincy@jones.com", true},
					PhoneNumber: "212 5555 5555",
				},
				validID: false,
				err:     ErrNoName,
			},
			"no tax ID": {
				c: &Customer{
					Name:        "Quincy Jones",
					TaxID:       "",
					Email:       sql.NullString{"quincy@jones.com", true},
					PhoneNumber: "212 5555 5555",
				},
				validID: false,
				err:     ErrNoTaxID,
			},
			"valid customer; no email": {
				c: &Customer{
					Name:        "Quincy Jones II",
					TaxID:       "111222333666",
					PhoneNumber: "212 5555 5555",
				},
				validID: true,
				err:     nil,
			},
			"no phone number": {
				c: &Customer{
					Name:        "Quincy Jones",
					TaxID:       "111222333444",
					Email:       sql.NullString{"quincy@jones.com", true},
					PhoneNumber: "",
				},
				validID: false,
				err:     ErrNoPhoneNumber,
			},
		}

		for k, tst := range tests {
			actualID, actualErr := AddCustomer(db, tst.c)
			if tst.validID && actualID == 0 || !tst.validID && actualID != 0 {
				errFmt := "AddCustomer [%s]: ID should be %s but is equal to %d"
				var validity string
				if tst.validID {
					validity = "valid"
				} else {
					validity = "zero"
				}
				t.Errorf(errFmt, k, validity, actualID)
			}
			if actualErr != tst.err {
				t.Errorf("AddCustomer [%s]: error should be %s but is %s", k, tst.err, actualErr)
			}
		}
	}
}
