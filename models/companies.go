package models

import (
	"upper.io/db.v3"
	"upper.io/db.v3/lib/sqlbuilder"
)

const tblCompanies = "companies"

type Company struct {
	Entity
	Name string `db:"name" json:"name"`
}

func Companies(db db.Database) db.Collection {
	return db.Collection(tblCompanies)
}

func GetCompanyByID(dbase db.Database, id int64) (c *Company, err error) {
	c = &Company{}
	err = GetEntityByID(Companies(dbase), id, c)
	return
}

func GetCompanyIDByName(dbase db.Database, name string) (int64, error) {
	m := map[string]int64{}
	if err := Companies(dbase).Find("name", name).Select("id").One(&m); err != nil && err != db.ErrNoMoreRows {
		return 0, err
	}
	return m["id"], nil
}

func InitializeCompanies(dbase sqlbuilder.Database, names []string) error {
	err := dbase.Tx(dbase.Context(), func(tx sqlbuilder.Tx) error {
		for _, n := range names {
			_, err := tx.InsertInto(tblCompanies).Columns("name").Values(n).Amend(func(queryIn string) string {
				return queryIn + " on conflict do nothing"
			}).Exec()
			if err != nil {
				return err
			}
		}
		return nil
	})
	return err
}
