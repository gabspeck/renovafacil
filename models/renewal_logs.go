package models

import (
	"errors"
	"github.com/guregu/null"
	"time"
	"upper.io/db.v3"
	"upper.io/db.v3/lib/sqlbuilder"
)

const tblRenewalLogs = "renewal_logs"

type RenewalLog struct {
	Entity
	PolicyID     int64     `db:"policy_id" json:"policyID"`
	NewAccountID null.Int  `db:"new_account_id" json:"newAccountID"`
	Date         time.Time `db:"date" json:"date"`
	Renewed      bool      `db:"renewed" json:"renewed"`
}

type RenewalHistoryEntry struct {
	ID            int64        `db:"id" json:"id"`
	Customer      string       `db:"customer" json:"customer"`
	OldAccount    string       `db:"old_account" json:"oldAccount"`
	Number        string       `db:"number" json:"number"`
	InsuranceType string       `db:"insurance_type" json:"insuranceType"`
	Description   string       `db:"description" json:"description"`
	NewAccount    *null.String `db:"new_account" json:"newAccount"`
	RenewedOn     time.Time    `db:"renewed_on" json:"renewedOn"`
	Total         int          `db:"total" json:"-"`
}

var (
	ErrNilRenewalLog  = errors.New("renewal log is nil")
	ErrBadPolicyID    = errors.New("policy ID is mandatory")
	ErrBadRenewalDate = errors.New("renewal log date is mandatory")
	ErrBadNewAccount  = errors.New("new account ID is mandatory for renewed policies")
)

func RenewalLogs(dbase sqlbuilder.Database) db.Collection {
	return dbase.Collection(tblRenewalLogs)
}

func AddRenewalLog(dbase sqlbuilder.Database, rl *RenewalLog) (int64, error) {
	if rl == nil {
		return 0, ErrNilRenewalLog
	}

	if rl.PolicyID < 1 {
		return 0, ErrBadPolicyID
	}

	if rl.Date.IsZero() {
		return 0, ErrBadRenewalDate
	}

	if rl.Renewed && rl.NewAccountID.Int64 < 1 {
		return 0, ErrBadNewAccount
	}

	if !rl.Renewed {
		rl.NewAccountID.Valid = false
	}

	newID, err := RenewalLogs(dbase).Insert(rl)
	if err != nil {
		return 0, err
	}
	return newID.(int64), err
}

func GetRenewalHistory(db sqlbuilder.Database, resultsPerPage, page uint) (total int, entries []RenewalHistoryEntry, err error) {
	actualPage := page - 1
	offset := actualPage * resultsPerPage
	r, err := db.Query(`select
	rl.id as id,
	c."name" as customer,
	oa."name" as old_account,
	p.policy_number as number,
	t."name" as insurance_type,
	p.description as "description",
	p.period_end as period_end,
	na."name" as new_account,
	rl."date" as renewed_on,
	count(rl.id) over() as total
from
	renewal_logs rl join policies p on
	p.id = rl.policy_id join customers c on
	c.id = p.customer_id join accounts oa on
	oa.id = p.account_id join insurance_types t on
	t.id = p.insurance_type_id left join accounts na on
	na.id = rl.new_account_id
order by
	rl."date" desc,
	rl.id desc,
	customer,
	customer_id limit ? offset ?`, resultsPerPage, offset)

	if err != nil {
		return
	}
	iter := sqlbuilder.NewIterator(r)
	defer iter.Close()
	if err = iter.All(&entries); err != nil {
		return
	}

	if len(entries) > 0 {
		total = entries[0].Total
	}

	return
}

func DeleteRenewalLogByID(dbase sqlbuilder.Database, id int64) error {
	return RenewalLogs(dbase).Find("id", id).Delete()
}
