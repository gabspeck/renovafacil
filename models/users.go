package models

import (
	"strings"

	"golang.org/x/crypto/bcrypt"
	"upper.io/db.v3"
	"upper.io/db.v3/lib/sqlbuilder"
)

const tblUsers = "users"

var (
	ErrNilUser                     = ValidationError("user is nil")
	ErrBlankUsername               = ValidationError("username is blank")
	ErrBlankFullName               = ValidationError("full name is blank")
	ErrBlankPasswordOrConfirmation = ValidationError("password and/or confirmation are blank")
	ErrConfirmationDoesntMatch     = ValidationError("password confirmation does not match password")
	ErrBadUsernameOrPassword       = ValidationError("bad username or password")
	ErrBadUserID                   = ValidationError("usuário inválido")
)

type User struct {
	Entity
	Username string `db:"username" json:"username"`
	FullName string `db:"full_name" json:"fullName"`
	Hash     []byte `db:"hash" json:"-"`
}

func Users(dbase sqlbuilder.Database) db.Collection {
	return dbase.Collection(tblUsers)
}

func AddUser(dbase sqlbuilder.Database, u *User, password, confirmation string) (int64, error) {
	if u == nil {
		return 0, ErrNilUser
	}

	if strings.TrimSpace(u.Username) == "" {
		return 0, ErrBlankUsername
	}

	if strings.TrimSpace(u.FullName) == "" {
		return 0, ErrBlankFullName
	}

	if strings.TrimSpace(password) == "" || strings.TrimSpace(confirmation) == "" {
		return 0, ErrBlankPasswordOrConfirmation
	}

	if password != confirmation {
		return 0, ErrConfirmationDoesntMatch
	}

	var err error
	u.Hash, err = bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return 0, err
	}

	id, err := Users(dbase).Insert(u)
	if err != nil {
		return 0, err
	}
	return id.(int64), nil
}

var AuthenticateUser = func(dbase sqlbuilder.Database, username, password string) (*User, error) {
	if strings.TrimSpace(username) == "" {
		return nil, ErrBlankUsername
	}

	u := &User{}
	err := Users(dbase).Find("username", username).One(u)
	if err == db.ErrNoMoreRows {
		return nil, ErrBadUsernameOrPassword
	}
	if err != nil {
		return nil, err
	}

	err = bcrypt.CompareHashAndPassword(u.Hash, []byte(password))
	if err == bcrypt.ErrMismatchedHashAndPassword {
		return nil, ErrBadUsernameOrPassword
	}
	if err != nil {
		return nil, err
	}

	return u, nil
}

var ChangeUserPassword = func(dbase sqlbuilder.Database, userID int64, newPassword, confirmation string) error {

	if newPassword == "" || confirmation == "" {
		return ErrBlankPasswordOrConfirmation
	}

	if newPassword != confirmation {
		return ErrConfirmationDoesntMatch
	}

	u := User{}
	if err := Users(dbase).Find("id", userID).One(&u); err != nil {
		if err == db.ErrNoMoreRows {
			return ErrBadUserID
		}
		return err
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(newPassword), bcrypt.DefaultCost)
	if err != nil {
		return err
	}

	copy(u.Hash, hash)

	if err := Users(dbase).UpdateReturning(&u); err != nil {
		return err
	}

	return nil
}
