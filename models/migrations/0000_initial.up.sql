create table companies
(
       id serial primary key,
       name varchar(50) unique not null check(btrim(name) <> '')

);
create table customers
(
       id serial primary key,
       name varchar(100) not null check(btrim(name) <> ''),
       tax_id varchar(20) unique not null check(btrim(tax_id) <> ''),
       email varchar(100) check(btrim(email) <> ''),
       phone_number varchar(30) check(btrim(phone_number) <> '')

);
create table insurance_types
(
       id serial primary key,
       name varchar(50) unique not null check (btrim(name) <> '')

);
create table policies
(
       id serial primary key,
       company_id integer not null references companies(id),
       customer_id integer not null references customers(id),
       insurance_type_id integer not null references insurance_types(id),
       policy_number varchar(30) not null check (btrim(policy_number) <> ''),
       description varchar(100) not null check (btrim(description) <> ''),
       period_start date not null,
       period_end date not null,
       unique(company_id, policy_number)
);
