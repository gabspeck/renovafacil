alter table policies
    drop column root_number,
    drop column endorsement_sequence;