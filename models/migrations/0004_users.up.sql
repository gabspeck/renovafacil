create table users (
    id serial primary key,
    username varchar(50) not null unique,
    full_name varchar(100) not null,
    hash bytea not null
);

-- Senha: corretora
insert into users (username, full_name, hash)
    values('admin','Administrador', decode('$2a$06$PoVgA3xDTMV33CzECFjoSO.rzr2Ow/zkwGEmLS5n3xtUTUxkSocKO', 'escape'));