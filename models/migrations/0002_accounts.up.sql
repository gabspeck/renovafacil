create table accounts(
    id serial primary key,
    company_id integer not null references companies(id),
    "name" varchar(50) unique not null check (btrim("name") <> ''),
    enabled boolean not null,
    username varchar(50) not null check (btrim(username) <> ''),
    "password" varchar(50) not null check (btrim("password") <> '')
);

insert into accounts (id, company_id, "name", enabled, username, "password")
select id, id, "name", true, 'CHANGE_ME', 'CHANGE_ME' from companies;

alter table policies
    rename company_id to account_id;

alter table policies
    drop constraint policies_company_id_fkey,
    add constraint policies_account_id_fkey foreign key (account_id) references accounts(id) match full;

