create
	view active_policies as select
		p.*
	from
		policies p
		left join policies q on
				(
					p.account_id = q.account_id
					and p.root_number = q.root_number
					and q.endorsement_sequence > p.endorsement_sequence
				)
    where q.id is null;

CREATE
	TABLE
		renewal_logs(
			id serial PRIMARY KEY,
			policy_id INTEGER NOT NULL UNIQUE REFERENCES policies(id),
			new_account_id INTEGER REFERENCES accounts(id),
			"date" DATE NOT NULL,
			renewed BOOLEAN NOT NULL
		);

