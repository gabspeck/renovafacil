drop table if exists companies cascade;
drop table if exists customers cascade;
drop table if exists insurance_types cascade;
drop table if exists policies cascade;
