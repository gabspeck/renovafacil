alter table policies
    rename account_id to company_id;
alter table policies
    drop constraint policies_account_id_fkey,
    add constraint policies_company_id_fkey foreign key (company_id) references companies(id) match full;

drop table accounts;