alter table policies
    add column root_number varchar not null default '',
    add column endorsement_sequence integer not null default 0;

begin;
update policies set
    root_number = substring(policy_number, 0, 16),
    endorsement_sequence = cast(substring(policy_number, 16) as integer)
where root_number = '' and company_id = (select id from companies c where c."name" = 'HDI');
end;

alter table policies
    alter column root_number drop default,
    add constraint policies_root_number_check CHECK (btrim(root_number) <> ''),
    alter column endorsement_sequence drop default;
