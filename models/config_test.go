package models

import (
	"os"
	"testing"
)

func TestParseConfig(t *testing.T) {
	const (
		eName     = "renovafacil"
		eHost     = "localhost"
		ePort     = "5433"
		eUser     = "aladdin"
		ePassword = "opensesame"
	)
	os.Setenv("DB_NAME", "renovafacil")
	os.Setenv("DB_HOST", "localhost")
	os.Setenv("DB_PORT", "5433")
	os.Setenv("DB_USER", "aladdin")
	os.Setenv("DB_PASSWORD", "opensesame")
	c, err := parseConfig()
	if err != nil {
		t.Fatalf("ParseConfig: unexpected error: %s", err)
	}
	db := c.DB
	if aName := db.Name; eName != aName {
		t.Errorf("wanted DB name %s, got %s", eName, aName)
	}
	if aHost := db.Host; eHost != aHost {
		t.Errorf("wanted DB host %s, got %s", eHost, aHost)
	}
	if aPort := db.Port; ePort != aPort {
		t.Errorf("wanted DB port %s, got %s", ePort, aPort)
	}
	if aUser := db.User; eUser != aUser {
		t.Errorf("wanted DB user %s, got %s", eUser, aUser)
	}
	if aPassword := db.Password; ePassword != aPassword {
		t.Errorf("wanted DB password %s, got %s", ePassword, aPassword)
	}
}
