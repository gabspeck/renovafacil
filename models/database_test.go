package models

import (
	"database/sql"
	"io/ioutil"
	"testing"

	"github.com/mattes/migrate"
	"upper.io/db.v3/lib/sqlbuilder"
)

func TestIntegration(t *testing.T) {
	db, err := Connect("postgres://renovafacil:renovafacil@localhost:6432/renovafacil?sslmode=disable", 1)
	if err != nil {
		t.Skipf("error connecting to database: %s; skipping integration tests", err)
	}

	if err := resetAndSeedDB(db); err != nil {
		t.Fatalf("error resetting & seeding database: %s", err)
	}

	t.Run("GetPendingRenewalsByPeriodEnd", testGetPendingRenewalsByPeriodEnd(db))
	t.Run("GetAccounts", testGetAccounts(db))
	t.Run("GetCompanyByID", testGetCompanyByID(db))
	t.Run("GetCompanyNameByID", testGetCompanyByID(db))
	t.Run("GetInsuranceTypeIDByName", testGetInsuranceTypeIDByName(db))
	t.Run("GetRenewalHistory", testGetRenewalHistory(db))
	t.Run("InitializeAccounts", testInitializeAccounts(db))
	t.Run("AddAccount", testAddAccount(db))
	t.Run("AddCustomer", testAddCustomer(db))
	t.Run("AddPolicy", testAddPolicy(db))
	t.Run("AddInsuranceType", testAddInsuranceType(db))
	t.Run("DeleteRenewalLogByID", testDeleteRenewalLogByID(db))
	t.Run("ChangeUserPassword", testChangeUserPassword(db))
}

func resetAndSeedDB(db sqlbuilder.Database) error {
	if err := resetDB(db.Driver().(*sql.DB)); err != nil {
		return err
	}

	if err := seedDB(db); err != nil {
		return err
	}

	return nil
}

func resetDB(db *sql.DB) error {
	m, err := newMigrate(db, "migrations")
	if err != nil {
		return err
	}
	if err := m.Down(); err != nil && err != migrate.ErrNoChange {
		return err
	}
	if err := m.Up(); err != nil && err != migrate.ErrNoChange {
		return err
	}

	return nil
}

func seedDB(db sqlbuilder.Database) error {
	f, err := ioutil.ReadFile("testdata/seed.sql")
	if err != nil {
		panic(err)
	}

	_, err = db.Exec(string(f))

	return err
}
