package models

import (
	"testing"

	"github.com/kylelemons/godebug/pretty"
	"golang.org/x/crypto/bcrypt"
	"upper.io/db.v3/lib/sqlbuilder"
)

func testChangeUserPassword(dbase sqlbuilder.Database) func(*testing.T){
	return func(t *testing.T){
		tests := map[string]struct{
			userID int64
			password string
			confirmation string
			err error
		}{
			"bad user ID": {
				userID: 9999,
				password: "password",
				confirmation: "password",
				err: ErrBadUserID,
			},
			"blank password": {
				userID: 1000,
				password: "",
				confirmation: "password",
				err: ErrBlankPasswordOrConfirmation,
			},
			"blank confirmation": {
				userID: 1000,
				password: "password",
				confirmation: "",
				err: ErrBlankPasswordOrConfirmation,
			},
			"confirmation mismatch": {
				userID: 1000,
				password: "password",
				confirmation: "confirmation",
				err: ErrConfirmationDoesntMatch,
			},
			"valid request": {
				userID: 1000,
				password: "password",
				confirmation: "password",
				err: nil,
			},
		}
		for k, test := range tests {
			actualErr := ChangeUserPassword(dbase, test.userID, test.password, test.confirmation)
			if d := pretty.Compare(test.err, actualErr); d != "" {
				t.Errorf("ChangeUserPassword [%s]: errors differ:\n%s", k, d)
			}
			if test.err == nil {
				u := User{}
				if err := Users(dbase).Find("id", test.userID).One(&u); err != nil {
					t.Errorf("ChangeUserPassword [%s]: error finding user for comparison: %s", k, err)
					continue
				}
				if err := bcrypt.CompareHashAndPassword(u.Hash, []byte(test.password)); err != nil {
					t.Errorf("ChangeUserPassword [%s]: password comparison error: %s", k, err)
				}
			}
		}
	}
}
