package models

import (
	"os"
)

type ClientParams map[string]string
type Config struct {
	DB struct {
		Name     string
		Host     string
		Port     string
		User     string
		Password string
	}
	Clients map[string]ClientParams
}

func NewConfig() (*Config, error) {
	return parseConfig()
}

func parseConfig() (*Config, error) {
	c := &Config{}
	c.DB.Name = os.Getenv("DB_NAME")
	c.DB.Host = os.Getenv("DB_HOST")
	c.DB.Port = os.Getenv("DB_PORT")
	c.DB.User = os.Getenv("DB_USER")
	c.DB.Password = os.Getenv("DB_PASSWORD")
	return c, nil
}

func NewClientParamsFromAccount(a *Account) ClientParams {
	return ClientParams{
		"username": a.Username,
		"password": a.Password,
	}
}
