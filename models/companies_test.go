package models

import (
	"testing"

	"github.com/kylelemons/godebug/pretty"
	"upper.io/db.v3/lib/sqlbuilder"
)

func testGetCompanyByID(db sqlbuilder.Database) func(*testing.T) {
	return func(t *testing.T) {
		tsts := map[string]struct {
			id      int64
			company *Company
			isErr   bool
		}{
			"existing ID": {
				id: 1000,
				company: &Company{
					Entity: Entity{ID: 1000},
					Name:   "Insurances 'R' Us",
				},
				isErr: false,
			},
			"bad ID": {
				id:      0,
				company: &Company{},
				isErr:   false,
			},
		}
		for k, tst := range tsts {
			actualCo, err := GetCompanyByID(db, tst.id)
			if hasErr := err != nil; hasErr != tst.isErr {
				t.Errorf("GetCompanyByID [%s]: bad error status: wanted %v, got %v (err: %s)", k, tst.isErr, hasErr, err)
			}
			if d := pretty.Compare(tst.company, actualCo); d != "" {
				t.Errorf("GetCompanyByID [%s]: companies differ:\n%s", k, d)
			}
		}
	}
}

func testGetCompanyIDByName(db sqlbuilder.Database) func(*testing.T) {
	return func(t *testing.T) {
		tsts := map[string]struct {
			name    string
			id      int64
			isError bool
		}{
			"existing company": {
				name:    "Felcher & Sons",
				id:      1000,
				isError: false,
			},
			"bad company": {
				name:    "i don't exist",
				id:      0,
				isError: true,
			},
		}
		for k, tst := range tsts {
			actualID, err := GetCompanyIDByName(db, tst.name)
			if hasErr := err != nil; hasErr != tst.isError {
				t.Errorf("GetCompanyIDByName [%s]: error status should be %v, is %v (err: %s)", k, tst.isError, hasErr, err)
			}
			if actualID != tst.id {
				t.Errorf("GetCompanyIDByName [%s]: ID should be %d, is %d", k, tst.id, actualID)
			}
		}
	}
}
